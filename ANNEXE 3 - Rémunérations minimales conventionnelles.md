# ANNEXE 3

## RÉMUNÉRATIONS MINIMALES CONVENTIONNELLES

### ANNEXE 3-1
AVENANT N°30 DU 20 OCTOBRE 2004 (E.T.A.M.)

### ANNEXE 3-2
AVENANT N°29 DU 5 OCTOBRE 2004 (I.C.)

## ANNEXE 3-3
ARRÊTÉ MINISTÉRIEL DU 11 JANVIER 2005 PORTANT EXTENSION DES AVENANTS N°30 DU 20 OCTOBRE 2004 ET N°29 DU 5 OCTOBRE 2004

## ANNEXE 3-4
CHARGÉS D’ENQUÊTE (C.E.) GRILLES DES RÉMUNÉRATIONS MINIMALES

## ANNEXE 3-1

### AVENANT N°30 DU 20 OCTOBRE 2004 À LA CONVENTION COLLECTIVE NATIONALE DU 15 DÉCEMBRE 1987 DES BUREAUX D’ETUDES TECHNIQUES, CABINETS D’INGÉNIEURS CONSEILS, SOCIÉTÉS DE CONSEILS

#### VALEURS DES APPOINTEMENTS MINIMAUX DES ETAM


Le présent Avenant vise à déterminer les salaires minimaux conventionnels des ETAM.

###### ARTICLE 1 -


Pour les Employés, Techniciens et Agents de Maîtrise (ETAM), les salaires minimaux conventionnels sont
déterminés de la manière suivante :
Positions 1.1 et 1.2 (coefficients 200 et 210) : Salaire minimum 1 195 Euros brut
Positions 1.3.1 et 1.3.2 (coefficients 220 et 230) : Salaire minimum 1 200 Euros brut
Position 1.4.1 et 1.4.2 (coefficient 240 et 250) : Salaire minimum 1 205 Euros brut

Pour les autres coefficients:
La valeur du point est fixée à 2,92 Euros brut et la partie fixe à 475 Euros brut ; les valeurs découlant de ce
calcul s'appliquent à partir de la position 2.1 (coefficient 275) :

| Positions | Coefficients | Salaires minimaux bruts |
| --------- | ------------ | ----------------------- |
| 2.1       | 275          | 1278 Euros              |
| 2.2       | 310          | 1380 Euros              |
| 2.3       | 355          | 1512 Euros              |
| 3.1       | 400          | 1643 Euros              |
| 3.2       | 450          | 1789 Euros              |
| 3.3       | 500          | 1935 Euros              |



###### ARTICLE 2 -


Les dispositions du présent avenant fixant les nouvelles valeurs des appointements minimaux
conventionnels, entreront en vigueur pour l’ensemble des entreprises de la Branche, adhérentes ou non
à une organisation patronale, le premier jour du mois civil suivant la publication de l’arrêté ministériel
d’extension du présent Avenant au Journal Officiel de la République Française dans le cadre du champ
d’application transitoire de la Convention Collective Nationale tel que défini par l’Accord du
21 novembre 1995 (JO du 21 février 1996).

### ANNEXE 3-2

AVENANT N°29 DU 5 OCTOBRE 2004 À LA CONVENTION COLLECTIVE NATIONALE DU 15 DÉCEMBRE 1987 DES BUREAUX D’ETUDES TECHNIQUES , CABINETS D’INGÉNIEURS CONSEILS ,SOCIÉTÉS DE CONSEILS

VALEURS DES APPOINTEMENTS MINIMAUX DES IC


Le présent Avenant vise à déterminer les salaires minimaux conventionnels des IC.

###### ARTICLE 1

La valeur du point des Ingénieurs et Cadres est fixée à 17,30 Euros bruts et ce à compter de la date prévue au deuxième article du présent avenant.

###### ARTICLE 2

Les dispositions du présent avenant fixant les nouvelles valeurs des appointements minimaux conventionnels, entreront en vigueur pour l’ensemble des entreprises de la Branche, adhérentes ou non à une organisation patronale, le premier jour du mois civil suivant la publication de l’arrêté d’extension du présent Avenant au Journal Officiel, ou au plus tard le 1er janvier 2005, dans le cadre du champ d’application transitoire de la Convention Collective Nationale tel que défini par l’Accord du 21 novembre 1995 (JO du 21 février 1996).

### ANNEXE 3-3

## ARRÊTÉ D’EXTENSION

###### ARRÊTÉ DU 11 JANVIER 2005 PORTANT EXTENSION D'AVENANTS À LA CONVENTION COLLECTIVE NATIONALE DES BUREAUX D'ÉTUDES TECHNIQUES, CABINETS D'INGÉNIEURS-CONSEILS, SOCIÉTÉS DE CONSEIL (N° 1486)

Le ministre de l'emploi, du travail et de la cohésion sociale,

Vu les articles L. 133-1 et suivants du code du
travail ;

Vu l'arrêté du 13 avril 1988 et les arrêtés successifs, notamment l'arrêté du 26 octobre 2004, portant extension de la convention collective nationale des bureaux d'études techniques, cabinets d'ingénieurs- conseils, sociétés de conseil du 15 décembre 1987 et de textes la complétant ou la modifiant ;

Vu l'avenant n° 29 du 5 octobre 2004 relatif aux valeurs des appointements minimaux des ingénieurs et cadres à la convention collective susvisée ;

Vu l'avenant n° 30 du 20 octobre 2004 relatif aux valeurs des appointements minimaux des employés, techniciens et agents de maîtrise à la convention collective susvisée ;

Vu les demandes d'extension présentées par les organisations signataires ;

Vu les avis publiés au Journal officiel des 10 et 16 décembre 2004 ;

Vu les avis recueillis au cours de l'enquête ;

Vu l'avis motivé de la Commission nationale de la négociation collective (sous-commission des conventions et accords), recueilli suivant la procédure prévue à l'article R. 133-2 du code du travail,

###### ARRETE :

###### ARTICLE 1ER -

Sont rendues obligatoires, pour tous les employeurs et tous les salariés compris dans le champ d'application de la convention collective nationale des bureaux d'études techniques, cabinets d'ingénieurs- conseils, sociétés de conseil du 15 décembre 1987 modifiée, les dispositions :

- de l'avenant n° 29 du 5 octobre 2004 relatif aux valeurs des appointements minimaux des ingénieurs et cadres à la convention collective susvisée ;
- de l'avenant n° 30 du 20 octobre 2004 relatif aux valeurs des appointements minimaux des employés, techniciens et agents de maîtrise à la convention collective susvisée.

###### ARTICLE 2 -


L'extension des effets et sanctions des avenants susvisés est faite à dater de la publication du présent arrêté pour la durée restant à courir et aux conditions prévues par lesdits avenants.

###### ARTICLE 3 -


Le directeur des relations du travail est chargé de l'exécution du présent arrêté, qui sera publié au Journal officiel de la République française.

Fait à Paris, le 11 janvier 2005.
Pour le ministre et par délégation : Par empêchement du directeur des relations du travail : Le sous-directeur de la négociation collective,
P. Florentin

### GRILLE DES RÉMUNÉRATIONS MINIMALES BRUTES DES CHARGÉS D’ENQUÊTE



1 - Les durées indiquées correspondent au temps nécessaire à l’interview, à l’exclusion du temps de recherche des enquêtes, préparation, relecture et expédition du travail.

2 - Les valeurs ci-dessous sont exprimées en points E.T.A.M.

3 - Les valeurs ci-dessous correspondent à l’exécution complète d’un questionnaire y compris la recherche de l’enquête, la préparation, la relecture et l’exécution du travail.

4 - Pour une enquête donnée, le nombre de points ou de fractions de points correspondant à un questionnaire est applicable à tout le personnel réalisant les questionnaires.

| Nature de l’enquête                                                     | Durée D'interview                       |
| ----------------------------------------------------------------------- | --------------------------------------- |
| 1. Lieu public                                                          | 10 min. 15 min.                         |
| 1.1. Lieu public non imposé                                             | 0,362 0,444                             |
| 1.2 Lieu public imposé (ex:exposition, magasins, kiosques, spectacles.) | 0.444 0.544                             |
| 2. Enquêtes à domicile                                                  | 15 min. 30 min. 45 min. 60 min. 90 min. |
| Coefficients                                                            | 1 1,45 1,90 2,35 3,25                   |
| 2.1 Ménagères, 2 quotas                                                 | 0,900 1,305 1,710 2,115 2,92            |
| 2.2. Ensemble, 2 quotas                                                 | 0,960 1,390 1,825 2,255 3,12            |
| 2.3. Hommes, 2 quotas                                                   | 1,020 1,480 1,940 2,395 3,31            |
| 2.4. 3è quota, coefficient 1                                            | 0,065 0,095 0,125 0,155 0,24            |
| 2.5. 4è quota, coefficient 1,15                                         | 0,075 0,110 0,140 0,175 0,24            |
| 2.6. 5è quota, coefficient (1,15) 2                                     | 0,085 0,125 0,165 0,200 0,28            |
| 3. Enquêtes sur adresse                                                 |                                         |
| 3.1. Moins de 20 p. 100 d’adresses erronées                             | 1,025 1,555 1,935 2,335 3,04            |
| 3.2. De 20 à 40 p. 100 d’adresses erronées                              | 1,335 1,780 2,335 2,670 3,20            |
| 3.3. Plus de 40 p. 100 d’adresses erronées                              |  1,780  2,335 2,670 3,000 3,30                                       |



1 - Quota

Par “quota”, il faut entendre :


a - Les répartitions imposées selon
des caractéristiques telles que : âge, catégories
socio-professionnelles, nombre de personnes au
foyer, femmes actives ou non.

b - Les conditions limitant la population
à enquêter, par exemple : possesseur ou
consommateur d’un bien déterminé. Les
coefficients de la grille s’appliquent lorsque la
limitation ainsi définie détermine une population
d’enquêtés d’au moins 50 % de la population
totale.

c - Lorsque le quota détermine une population
d’enquêtés inférieure à 50 % de la population
totale, les conditions de rémunération font l’objet
d’un examen particulier.

2 - La grille ci-jointe recouvre la très grande majorité
des enquêtes. Elle donne des valeurs minimales
des rémunérations qui constituent des éléments de
référence pour la détermination des rémunérations
d’une enquête déterminée.

Ces chiffres multipliés par la valeur du point
donnent la valeur en francs du questionnaire
correspondant. Les valeurs du questionnaire
incluent la rémunération des opérations de
recherche des enquêtés, passation du questionnaire,
préparation, relecture et expédition du travail.


3 - Les travaux annexes, autres que ceux dont la
rémunération est incluse dans les valeurs du
questionnaire, sont rémunérés sur la base du
coefficient hiérarchique correspondant de la
classification.

4 - Lorsque le délai de retour des questionnaires
est inférieur ou égal à quarante-huit heures, il en
est tenu compte dans la rémunération.

5 - L’utilisation pour les besoins de l’enquête d’un
matériel lourd et encombrant donnera lieu à une
majoration.

6 - Le rattrapage des interviews sur adresses ayant
donné lieu à un refus est rémunéré comme les
travaux annexes visés ci-dessus en 3.

7 - Il en est de même des opérations de prise de
rendez-vous sur instructions de l’employeur.
