# Convention collective nationale des bureaux d'études techniques, des cabinets d'ingénieurs-conseils et des sociétés de conseils du 15 décembre 1987.

# Déclaration liminaire

# Article

_En vigueur étendu_

Les parties signataires le 15 décembre 1987 de la nouvelle convention collective nationale des bureaux d'études techniques, cabinets d'ingénieurs-conseils et sociétés de conseils s'engagent à revoir d'un commun accord les articles dont la rédaction devrait être précisée eu égard notamment aux dispositions légales en vigueur.

# Préambule

# Bureaux d'ingénieurs-conseils relevant des syndicats de la chambre des ingénieurs-conseils de France

# Article

_En vigueur étendu_

Les organisations contractantes reconnaissent que les bureaux d'études et cabinets d'ingénieurs-conseils relevant des syndicats de la chambre des ingénieurs-conseils de France, par leur structure et leur activité, comportent pour ceux qui y travaillent des particularités communes à la plupart des professions libérales.

La CICF étant membre de la confédération générale des petites et moyennes entreprises est tenue par les accords interprofessionnels signés par cet organisme et non par les accords signés par le Conseil national du patronat français dont elle ne fait pas partie.

Leur activité libérale, caractérisée en particulier par leur adhésion à la chambre des ingénieurs-conseils de France et à l'Union nationale des professions libérales, implique entre le " patron " et ses collaborateurs une étroite solidarité, pour donner au client le service personnalisé qu'il en attend.

La présente convention s'applique à tous les adhérents des syndicats d'employeurs contractants quelle que soit la forme juridique sous laquelle ils exercent leur activité.

# Bureaux d'études, aux bureaux d'ingénieurs-conseils et aux sociétés de conseil relevant de la fédération des syndicats des sociétés d'études et de conseils

# Article

_En vigueur étendu_

Dernière modification : Modifié par avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier
1992

Les organisations contractantes reconnaissent que les bureaux d'études, les bureaux d'ingénieurs-conseils et les sociétés de conseils relevant de la fédération des syndicats des sociétés d'études et de conseils (Syntec), par leur structure comme par la nature de leur activité, présentent des caractéristiques très particulières comportant pour ceux qui y travaillent à quelque titre que ce soit des avantages et des risques qu'il s'agit d'équilibrer aussi harmonieusement que possible.

Elles sont d'accord pour admettre :

- que le service de ces organismes n'a de sens et de justification que s'il s'agit d'un service de très haute qualité ;

- que chacun d'eux est essentiellement une équipe organisée et hiérarchisée dont le dynamisme et l'efficacité sont liés à sa cohésion interne et à l'esprit de coopération dont font preuve ceux qui la composent ;

- que la présente convention s'applique à tous les adhérents de la fédération des syndicats, quelle que soit la forme juridique sous laquelle ils exercent leur activité.

# Bureaux d'études techniques, cabinets d'ingénieurs-conseils et sociétés de conseils membres de la CICF et de Syntec

# Article

__En vigueur étendu__

Les parties signataires déclarent que les sociétés relevant de la profession de l'ingénierie et du conseil ont la
particularité commune de prendre en charge des interventions d'études et de réalisation très diverses :

- dans leur ampleur : de quelques journées de travail à plusieurs années d'activité pour des équipes complètes ;

- dans leur technicité, une même intervention pouvant exiger des spécialistes de profil pointu dans des disciplines variées ;

- dans leur localisation, la France entière et le monde entier ;

- dans le temps, les dates de déroulement et la durée d'une intervention étant variables et souvent susceptibles d'être remises en cause. et que, par conséquent, pour faire face à ces réalités dans les meilleures conditions d'efficacité et de compétitivité, ces sociétés proposent pour certains de leurs emplois des contrats spécifiant une mission d'intervention.

# Personnels enquêteurs

# Article

_En vigueur étendu_

L'activité des instituts de sondages présente un caractère très particulier : les variations de la répartition géographique de la demande, tant en volume qu'en nature, les impératifs de souplesse et de rapidité qui sont indispensables dans de nombreux cas, ne permettent pas à ces sociétés d'assurer à l'ensemble de leurs enquêteurs une charge de travail régulière et constante au cours de l'année, eu égard de plus au fait qu'il est impératif d'obtenir, pour des nécessités statistiques, des échantillons dispersés.

Compte tenu de ces particularités, trois statuts différents sont proposés : le premier est intégré à la présente convention, les deux autres sont définis en annexe.

Le premier est celui de chargés d'enquête, titulaires d'un contrat à durée indéterminée qui les place sous la subordination exclusive d'un employeur, ces collaborateurs sont des salariés à plein temps qui doivent effectuer toutes les enquêtes qui leur sont demandées dans le cadre des règles définies ci-après. Ils relèvent de la catégorie ETAM. Leur situation offre simplement une originalité, qui tient au mode de calcul de leur rémunération : celle-ci est variable puisqu'elle est fonction du nombre et de la nature des enquêtes accomplies. Elle est nécessairement supérieure ou égale à un minimum mensuel.

Le deuxième est un statut de chargés d'enquête à garantie annuelle. Il s'agit de personnes engagées en vue d'une activité discontinue. La situation de ces enquêteurs se distingue de celle des chargés d'enquête en ce qu'ils ne s'engagent pas de manière exclusive à l'égard d'un employeur : il ne leur est pas interdit d'exercer d'autres activités ou la même activité au profit d'un autre organisme de sondage dans le cadre des règles définies en annexe. Les contrats de travail des chargés d'enquête sont soit à durée indéterminée, soit à durée déterminée.

Le troisième est celui d'enquêteurs vacataires. Ces derniers sont des collaborateurs occasionnels qui ont la possibilité de refuser les enquêtes qui leur sont proposées.

Lorsqu'ils les acceptent, ils ne sont liés par contrat à l'organisme de sondage que pour la durée d'exécution des tâches confiées. L'engagement n'est pas exclusif : il ne leur est pas interdit d'exercer d'autres activités, ou la même activité au profit d'un autre organisme de sondage.

# Titre Ier : Généralités

# Champ professionnel d'application

# Article 1er

_En vigueur étendu_

_Dernière modification : Modifié par avenant n° 37 du 28 octobre 2009_

La présente convention définit le statut des membres du personnel des entreprises ayant notamment pour
codes NAF ceux mentionnés dans le présent avenant et dont l'activité principale est une activité d'ingénierie,
de conseil, de services informatiques, des cabinets d'ingénieurs-conseils, des entreprises d'organisation de
foires et salons, entreprises dont le siège social ou les activités se situent en France métropolitaine ou dans
les départements d'outre-mer et les territoires d'outre-mer.

Le champ d'application de la convention collective nationale est le suivant, conformément à la nouvelle
nomenclature des activités économiques :

Informatique

58. 21Zp : édition de jeux électroniques.

59. 29Ap : édition de logiciels système et de réseau.

60. 29Bp : édition de logiciels outils de développement et de langages.

61. 29Cp : édition de logiciels applicatifs.

62. 01Zp : programmation informatique.

63. 02Ap : conseil en systèmes et logiciels informatiques.

64. 02B : tierce maintenance de systèmes et d'applications informatiques.

65. 09Zp : autres activités informatiques.

66. 03Z : gestion d'installations informatiques.

67. 11Zp : traitement de données, hébergement et activités connexes.

68. 12Zp : édition de répertoires et de fichiers d'adresses.

69. 12Z : portails internet.

Ingénierie

71. 12Bp : ingénierie, études techniques.

72. 90Bp : activités spécialisées, scientifiques et techniques diverses.

73. 20B : analyses, essais et inspections techniques.

Études et conseil

73. 20Z : études de marché et sondages.

74. 21Z : conseil en relations publiques et communication.

75. 22Zp : conseil pour les affaires et autres conseils de gestion.

76. 10Zp : activités des agences de placement de main-d'oeuvre.

77. 30Z : autre mise à disposition de ressources humaines.

Foires, congrès et salons

82. 30Z : organisation de foires, salons professionnels et congrès.

83. 32C : agencement de lieux de vente, montage de stands.

84. 11Z : fabrication de structures métalliques et éléments modulaires pour exposition.

85. 04Z : gestion de salles de spectacles.

86. 32A : administration d'immeubles et autres bien immobiliers.

87. 20B : location de terrains et autres biens immobiliers : halls d'exposition, salles de conférence, de réception, de réunion.

Traduction et interprétation

74. 30F : traduction et interprétation.

# Définition des ETAM, des CE et des IC

# Article 2

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Pour l'application des dispositions de la présente convention collective, sont considérés :

a) Comme ETAM, les salariés dont les fonctions d'employés, de techniciens ou d'agents de maîtrise sont définies en annexe par la classification correspondante.

b) Comme CE, les enquêteurs qui ont perçu d'une part, pendant deux années consécutives, une rémunération annuelle au moins égale au minimum annuel garanti définie à l'article 32 CE ci-après et, d'autre part, ayant fait la preuve de leur aptitude à effectuer de manière satisfaisante tous types d'enquêtes dans toutes les catégories de la population.

Les enquêteurs peuvent refuser le bénéfice de ce statut. L'employeur peut proposer ce statut même si ces conditions ne sont pas remplies.

c) Comme IC, les ingénieurs et cadres diplômés ou praticiens dont les fonctions nécessitent la mise en oeuvre de connaissances acquises par une formation supérieure sanctionnée par un diplôme reconnu par la loi, par une formation professionnelle ou par une pratique professionnelle reconnue équivalente dans notre branche d'activité.

Les fonctions d'ingénieurs ou cadres sont définies en annexe par la classification correspondante.

Ne relèvent pas de la classification ingénieurs ou cadres, ni des dispositions conventionnelles spécifiques à ces derniers, mais relèvent de la classification ETAM, les titulaires des diplômes ou les possesseurs d'une des formations précisées ci-dessus, lorsqu'ils n'occupent pas aux termes de leur contrat de travail des postes nécessitant la mise en oeuvre des connaissances correspondant aux diplômes dont ils sont titulaires.

Ne relèvent pas non plus de la classification ingénieurs ou cadres, mais relèvent de la classification ETAM, les employés, techniciens ou agents de maîtrise cotisant à une caisse des cadres au titre des articles IV bis et 36 de la convention collective de retraite des cadres du 14 mars 1947.

# Droit syndical et liberté d'opinion

# Article 3

_En vigueur étendu_

Dernière modification : Modifié par avenant du 25 octobre 2007

L'exercice du droit syndical est reconnu dans toutes les entreprises et s'effectue conformément aux dispositions légales en vigueur.

1. Les parties contractantes reconnaissent le droit pour tous de s'associer et d'agir librement pour la défense collective de leurs intérêts professionnels.

L'entreprise étant un lieu de travail, les employeurs pour eux et pour leurs représentants, s'engagent :

A ne pas prendre en considération le fait que les membres du personnel appartiennent ou non à un syndicat, exercent ou non des fonctions syndicales ;

A ne pas prendre de décisions discriminatoires en ce qui concerne l'embauchage, la conduite et la répartition du travail, la rémunération et l'octroi d'avantages sociaux, les mesures disciplinaires, l'avancement ou le licenciement, en raison de leur sexe, leur situation de famille, leurs origines sociales ou raciales, leur handicap, leurs opinions ou confessions.

Ils s'engagent également à ne faire aucune pression sur le personnel en faveur de tel ou tel syndicat.

Les salariés s'engagent de leur côté à ne pas prendre en considération dans le travail les opinions de leurs collègues et du personnel avec lequel ils sont en rapport, leur adhésion à tel ou tel syndicat, ou le fait de n'appartenir à aucun syndicat.

Les parties contractantes s'engagent à veiller à la stricte observation des engagements définis ci-dessus et à s'employer auprès de leurs ressortissants respectifs pour en assurer le respect intégral.

Si le bien-fondé d'un licenciement est contesté parce que ce licenciement aurait été effectué en violation du droit syndical, tel qu'il vient d'être défini ci-dessus, les deux parties s'emploieront à reconnaître les faits et à apporter au cas litigieux une solution équitable.

Cette intervention ne fait pas obstacle au droit pour les parties d'obtenir judiciairement réparation du préjudice causé.

2. Des absences non rémunérées ou prélevées sur le crédit d'heures dont ils peuvent disposer (1) seront accordées aux salariés mandatés par leurs organisations syndicales pour participer d'une part à leurs réunions statutaires et d'autre part à une réunion préparatoire à une réunion de la commission paritaire.

Ils devront en faire la demande au moins 8 jours à l'avance et produire un document desdites organisations.

3. Lorsque les salariés seront appelés à participer aux réunions paritaires décidées entre les employeurs et les organisations syndicales représentatives au niveau national, des autorisations d'absence seront accordées, les heures correspondantes rémunérées et non décomptées sur les congés payés dans la limite d'un nombre de salariés fixés d'un commun accord par les employeurs et les organisations syndicales représentatives au niveau national.

Le nombre de salariés d'une même entreprise autorisés à s'absenter simultanément sera fixé d'un commun accord par les employeurs et les organisations syndicales représentatives au niveau national.

Les employeurs et les organisations syndicales représentatives au niveau national en cause s'efforceront, dans les cas visés aux 2 et 3 ci-dessus, de faire en sorte que ces absences n'apportent pas de gêne appréciable à la marche générale de l'entreprise ou au libre exercice du droit syndical.

4. L'affichage des communications syndicales s'effectue librement sur des panneaux réservés à cet usage et distincts de ceux qui sont affectés aux communications des délégués du personnel et du comité d'entreprise.

   Un exemplaire de ces communications syndicales est transmis au chef d'entreprise simultanément à l'affichage.

Les panneaux sont mis à la disposition de chaque section syndicale suivant les modalités fixées par accord avec le chef d'entreprise.

Les publications et tracts de nature syndicale peuvent être librement diffusés aux travailleurs de l'entreprise, dans l'enceinte de celle-ci, aux heures d'entrée et de sortie du travail.

Ces communications, publications et tracts doivent avoir exclusivement pour objet l'étude et la défense des intérêts des salariés et ne doivent revêtir aucun caractère injurieux, diffamatoire.

La direction et les délégués syndicaux prendront en commun toutes les dispositions utiles pour assurer, au moins semestriellement au personnel en mission de longue durée pour raisons de travail, la transmission de l'information syndicale propre à l'entreprise.

La collecte des cotisations syndicales peut être effectuée à l'intérieur de l'entreprise.

(1) Termes exclus de l'extension (arrêté du 13 avril 1988, art. 1er).

# Délégués du personnel et comité d'entreprise

# Article 4

_En vigueur étendu_

La représentation des salariés par les délégués du personnel et aux comités d'entreprise est réglée par les dispositions législatives en vigueur.

Cependant, lorsque dans une entreprise de plus de 25 salariés les ingénieurs et cadres sont au moins au nombre de 15, il sera constitué un collège électoral spécial (1).

Les délégués du personnel pourront, dans les réunions avec l'employeur, se faire assister d'un représentant d'une organisation syndicale. De son côté, l'employeur pourra se faire assister d'un représentant de l'organisation patronale (2). Dans ce cas, ils devront s'en avertir réciproquement au moins 24 heures à l'avance.

Dans le cas où il serait impossible dans certaines entreprises d'appliquer les dispositions légales assurant des ressources stables aux comités d'entreprise, faute de trouver des bases de référence dans les 3 années précédant la prise en charge des oeuvres sociales par le comité d'entreprise, l'importance et la forme de participation de l'employeur au financement des oeuvres sociales feront l'objet dans les entreprises intéressées d'une négociation paritaire.

(1) Alinéa étendu sous réserve de l'application des articles L. 423-3 et L. 433-2, alinéa 5, du code du travail (arrêté du 13 avril 1988, art. 1er).

(2) Termes exclus de l'extension (arrêté du 13 avril 1988, art. 1er).

# Titre II : Conditions d'engagement

# Engagement et contrat de travail

# Article 5

_En vigueur étendu_

Il sera remis à tout collaborateur au moment de son engagement un contrat de travail, comportant notamment les indications suvantes :

- durée du contrat ;
- date d'entrée dans l'entreprise ;
- fonction occupée par l'intéressé ;
- classification et coefficient hiérarchique ;
- lieu d'emploi ;
- conditions d'essai ;
- horaires de référence ;
- montant du salaire mensuel ou conditions de rémunération pour les CE ;
- autres éléments éventuels de rémunération directs ou indirects ;
- clause de mobilité géographique le cas échéant.

Tout candidat à un emploi doit satisfaire à l'examen médical d'embauche. Si cet examen n'est effectué qu'au cours de la période d'essai et qu'il révèle une inaptitude à l'emploi considéré, l'employeur devra néanmoins respecter les dispositions relatives au préavis pendant la période d'essai.

Le texte de la convention collective sera communiqué à tout candidat retenu qui le demandera.

# Offres d'emploi

# Article 6

_En vigueur étendu_

a) En cas de vacance ou de création de poste, les employeurs feront appel par priorité aux personnels employés dans l'entreprise, susceptibles d'occuper le poste.

b) Les employeurs feront connaître leurs offres d'emploi à l'ANPE (Agence nationale pour l'emploi) et à l'APEC (Agence pour l'emploi des cadres).

# Période d'essai

# Article 7

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

### ETAM :

Dans la lettre d'engagement ou le contrat de travail, tout employé, technicien ou agent de maîtrise est soumis à une période d'essai dont la durée pourra être prolongée exceptionnellement d'une période équivalente, après accord écrit du salarié.

Cette durée est fonction de la classification conventionnelle du salarié :

- du coefficient 200 au coefficient 355 inclus, la période d'essai sera de 1 mois renouvelable aux conditions prévues ci-dessus ;

- du coefficient 400 au coefficient 500 inclus, la période d'essai sera de 2 mois renouvelable aux conditions prévues ci-dessus.

La période d'essai ne sera pas observée dans les cas de réintégration prévue par la loi ou la convention collective.

### IC :

Sauf accord entre les parties précisé dans la lettre d'engagement ou le contrat de travail, tout ingénieur ou cadre est soumis à une période d'essai de trois mois qui pourra être prolongée exceptionnellement d'une période de même durée, après accord écrit du salarié.

La période d'essai ne sera pas observée dans les cas de réintégration prévus par la loi ou la convention collective.

# Modification du contrat en cours

# Article 8

_En vigueur étendu_

a) Toute modification apportée à une clause substantielle du contrat en cours d'un salarié doit faire l'objet d'une notification écrite de la part de l'employeur.

b) Si cette modification n'est pas acceptée par l'intéressé, elle équivaut à un licenciement du fait de l'employeur et doit être réglée comme tel.

c) Par contre, si par suite de circonstances particulières résultant de la situation du travail dans l'entreprise, un salarié se trouve amené à assumer temporairement, dans des conditions de durée précisées à l'avance par écrit, n'excédant pas six mois, et sans diminution de sa classification ni diminution de ses appointements, une fonction inférieure à celle qu'il assume habituellement, le refus de l'intéressé d'accepter cette fonction temporaire équivaut à une démission de sa part.

# Modification dans la situation juridique de l'employeur

# Article 9

_En vigueur étendu_

S'il survient une modification dans la situation juridique de l'employeur, tous les contrats individuels de travail en cours au jour de la modification subsistent entre le nouvel employeur et les salariés de l'entreprise, conformément à l'article L. 122-12 du code du travail.

# Contrats à durée déterminée

# Article 10

_En vigueur étendu_

La présente convention collective est applicable aux salariés sous contrat à durée déterminée dans le cadre de la législation en vigueur.

# Travail à temps partiel

# Article 11

_En vigueur étendu_

Lorsqu'un salarié est employé à temps partiel, les conditions de son emploi et de sa rémunération sont spécifiées dans sa lettre d'engagement ou dans tout avenant ultérieur.

La présente convention collective lui est applicable dans le cadre de la législation en vigueur.

# Ancienneté

# Article 12

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier
1992

On entend par ancienneté le temps passé dans l'entreprise, c'est-à-dire le temps pendant lequel le salarié a été employé en une ou plusieurs fois quels qu'aient été ses emplois successifs. Déduction est faite toutefois en cas d'engagements successifs de la durée des contrats dont la résiliation est imputable à la démission de l'intéressé, sauf décision contraire de l'employeur, ou à une faute grave commise par le salarié ayant entraîné son licenciement.

Pour les CE on entend par ancienneté le temps d'activité exclusive et régulière exercée pour le compte de l'institut. Il peut s'y ajouter le temps de la période de référence définie à l'article 2 b).

Seront en outre prises en compte toutes les années pendant lesquelles l'enquêteur aura reçu onze bulletins de salaire sur douze et aura perçu au moins trois fois la valeur du SMIC.

Les interruptions pour mobilisation ou faits de guerre entrent intégralement en compte pour la détermination du temps d'ancienneté. Il en est de même des interruptions pour :

- périodes militaires obligatoires dans la réserve ;

- maladies, accidents ou maternités (à l'exclusion des périodes d'incapacité de travail ininterrompue supérieure ou égale à 6 mois pendant lesquelles le contrat de travail est suspendu) ;

- congés de formation ;

- congés annuels ou congés exceptionnels de courte durée résultant d'un commun accord entre les parties ;

- détachements auprès d'une filiale ;

- les autres interruptions du contrat donnant droit, selon les dispositions du code du travail, au maintien à tout ou partie de l'ancienneté.

# Titre III : Résiliation du contrat de travail

# Dénonciation du contrat de travail

# Article 13

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Toute résiliation du contrat de travail implique de part et d'autre un préavis, sauf cas de faute lourde, de faute grave ou de force majeure.

La résiliation du contrat de travail par l'une ou l'autre des parties est notifiée par lettre recommandée avec demande d'avis de réception dont la date de première présentation constitue la date de notification de la dénonciation du contrat. Si nécessaire, cette disposition devra être adaptée dans le cas particulier des salariés à l'étranger.

La lettre de résiliation du contrat de travail se référera, s'il y a lieu, aux stipulations du contrat de travail ou de toute autre pièce faisant état de clauses particulières. Elle rappellera la fonction exercée dans l'entreprise par le salarié et la durée du préavis qui lui est applicable en vertu de son contrat ou de la présente convention.

Tout salarié licencié, quels que soit son ancienneté, la taille de l'entreprise et le motif du licenciement, sera convoqué par l'employeur à un entretien préalable.

La convocation à cet entretien sera effectuée par lettre recommandée avec demande d'accusé de réception ou par lettre remise en mains propres contre décharge indiquant l'objet de la convocation et rappelant que le salarié peut se faire assister par une personne de son choix appartenant au personnel de l'entreprise ou par une personne extérieure inscrite sur une liste établie par le préfet quand il n'y a pas de représentant du personnel dans l'entreprise.

Seuls seront exclus du champ d'application de l'entretien préalable les salariés inclus dans un projet de licenciement économique concernant dix salariés et plus dans la même période de trente jours, ce licenciement faisant l'objet d'une consultation des représentants du personnel.

Le licenciement du salarié est notifié selon les modalités prévues au second alinéa du présent article.

# Préavis pendant la période d'essai

# Article 14

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Au cours de cette période, les deux parties peuvent se séparer avec un préavis d'une journée de travail pendant le premier mois. Après le premier mois, le temps de préavis réciproque sera d'une semaine par mois complet passé dans l'entreprise.

Après le premier mois, le temps de préavis réciproque sera d'une semaine par mois passé dans l'entreprise.

Le préavis donne droit au salarié de s'absenter pour la recherche d'un emploi dans les conditions fixées à l'article 16.

Le salarié sera payé au prorata du temps passé pendant la période d'essai.

# Préavis en dehors de la période d'essai

# Article 15

_En vigueur étendu_

Dernière modification : Complété par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

### ETAM :

La durée du préavis, dite aussi "délai-congé", est de 1 mois, quelle que soit la partie qui dénonce le contrat, sauf accord entre les parties prévoyant une durée supérieure.

Après 2 ans d'ancienneté, la durée du préavis ne doit pas être inférieure à 2 mois.

Le préavis n'est pas dû en cas de faute grave ou lourde du salarié.

Pour les ETAM classés aux coefficients hiérarchiques conventionnels 400, 450 et 500, le préavis réciproque sera de 2 mois quelle que soit leur ancienneté acquise.

### IC :

Sauf accord entre les parties prévoyant une durée supérieure, la durée du préavis, dite aussi "délai-congé", est de 3 mois, quelle que soit la partie qui dénonce le contrat.

Le préavis n'est pas dû en cas de faute grave ou lourde du salarié.

# Absence pour recherche d'emploi pendant la période de préavis

# Article 16

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

a) Pendant la période d'essai, la durée des absences autorisées pour la recherche d'emploi doit être calculée sur la base de 2 heures pour chaque jour ouvré compté entre la date de démission ou de licenciement, d'une part, et la fin de l'activité du salarié dans l'entreprise, d'autre part.

b) En dehors de la période d'essai, pendant la durée conventionnelle ou contractuelle du préavis, les salariés ont le droit de s'absenter pour recherche d'emploi pendant 6 jours ouvrés par mois, pris chaque mois en une ou plusieurs fois, en principe par demi-journée. Les heures d'absence seront fixées moitié au gré de l'employeur et moitié au gré du salarié moyennant avis réciproque. Une attention particulière sera portée aux salariés licenciés pour raison économique.

c) ETAM et IC :

Dans les deux cas, il est spécifié que ces absences ne donnent pas lieu à réduction de rémunération pour les salariés licenciés. En revanche, les heures d'absence pour recherche d'emploi des salariés démissionnaires ne donnent pas lieu à rémunération. En outre, aucune indemnité particulière n'est due au salarié licencié qui n'utilise pas ces heures d'absence pour recherche d'emploi.

d) CE :

Dans les deux cas, il est spécifié que ces absences ne donnent pas lieu à réduction du minimum garanti pour les chargés d'enquête licenciés.

En revanche, les heures d'absence pour recherche d'emploi des chargés d'enquête démissionnaires ne donnent pas lieu à rémunération.

En outre, aucune indemnité particulière n'est due au chargé d'enquête licencié qui n'utilise pas ses possibilités d'absence.

# Indemnité compensatrice de préavis

# Article 17

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Sauf accord contraire entre les parties, et hormis le cas de faute grave, la partie qui n'observerait pas le préavis devrait à l'autre une indemnité égale à la rémunération correspondant à la durée du préavis restant à courir : cette rémunération comprendra tous les éléments contractuels du salaire.

En cas de licenciement, le salarié pourra quitter son emploi dès qu'il sera pourvu d'une nouvelle place. Dans ce cas, il n'aura droit, indépendamment de l'indemnité éventuelle de licenciement, qu'à la rémunération correspondant à la durée de la période de préavis effectivement travaillée.

De même, l'employeur pourra exiger le départ immédiat du salarié licencié. Dans ce cas, l'indemnité compensatrice de préavis comme fixée ci-dessus, ainsi que toute indemnité éventuellement due à l'intéressé en application de la présente convention et de son contrat personnel, seront payées immédiatement en totalité, à la demande du salarié.

# Indemnité de licenciement - Conditions d'attribution

# Article 18

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Il est attribué à tout salarié licencié justifiant d'au moins 2 années d'ancienneté une indemnité de licenciement distincte de l'indemnité éventuelle de préavis.

Cette indemnité de licenciement n'est pas due dans le cas où le licenciement est intervenu pour faute grave ou lourde.

Cette indemnité sera réduite de 1/3 lorsque le salarié sera pourvu par l'employeur, avant la fin de la période de préavis, d'un emploi équivalent et accepté par l'intéressé en dehors de l'entreprise.

Ce tiers restant sera versé à l'intéressé si la période d'essai dans le nouvel emploi reste sans suite.

# Montant de l'indemnité de licenciement

# Article 19

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 18 du 25 janvier 1996 BO conventions collectives 96-6 étendu par arrêté du 20 juillet 1998 JORF 4 août 1998.

### ETAM :

L'indemnité de licenciement se calcule en mois de rémunération sur les bases suivantes :

- pour une ancienneté acquise entre 2 ans et 20 ans : 0,25 mois par année de présence ;

- à partir de 20 ans d'ancienneté : 0,30 mois par année de présence, sans pouvoir excéder un plafond de 10 mois.

Le mois de rémunération s'entend dans le cas particulier comme le douzième de la rémunération des 12 derniers mois précédant la notification de la rupture du contrat de travail, cette rémunération incluant les primes prévues par les contrats de travail individuels et excluant les majorations pour heures supplémentaires au-delà de l'horaire normal de l'entreprise et les majorations de salaire ou indemnités liées à un déplacement ou un détachement. Pour les années incomplètes, l'indemnité de licenciement est calculée proportionnellement au nombre de mois de présence.

En cas d'engagements successifs et de prise en compte de l'ancienneté dans les conditions prévues à l'article 12, l'indemnité de licenciement qui aura pu être perçue à l'occasion d'un licenciement antérieur est déductible de l'indemnité de licenciement prévue par le présent article.

### IC :

L'indemnité de licenciement se calcule en mois de rémunération sur les bases suivantes :

Après 2 ans d'ancienneté, 1/3 de mois par année de présence de l'ingénieur ou du cadre, sans pouvoir excéder un plafond de 12 mois.

Le mois de rémunération s'entend dans le cas particulier comme 1/12 de la rémunération des 12 derniers mois précédant la notification de la rupture du contrat de travail, cette rémunération incluant les primes prévues par les contrats de travail individuels et excluant les majorations pour heures supplémentaires au- delà de l'horaire normal de l'entreprise et les majorations de salaire ou indemnités liées à un déplacement ou un détachement. Pour les années incomplètes, l'indemnité de licenciement est calculée proportionnellement au nombre de mois de présence.

En cas d'engagements successifs et de prise en compte de l'ancienneté dans les conditions prévues à l'article 12, l'indemnité de licenciement qui aura pu être perçue à l'occasion d'un licenciement antérieur est déductible de l'indemnité de licenciement prévue par le présent article.

# Départ en retraite et mise à la retraite

# Article 20

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 28 du 28 avril 2004 art. 1 en vigueur à l'extension BO conventions collectives 2004-21 étendu par arrêté du 16 juillet 2004 JORF 28 juillet 2004.

1. Mise à la retraite

L'employeur a la possibilité de mettre fin au contrat de travail en procédant à la mise à la retraite de salariés âgés de plus de 60 ans, dès lors que ceux-ci, lors de leur départ de l'entreprise, remplissent les conditions pour bénéficier d'une retraite à taux plein du régime d'assurance vieillesse.

En contrepartie, l'employeur s'engage à procéder dans les 6 mois précédant ou suivant la notification de la mise à la retraite, à une embauche compensatrice en contrat à durée indéterminée pour la mise à la retraite de deux salariés âgés de 60 à 65 ans.

Par ailleurs, le montant de l'indemnité de mise à la retraite est calculé dans les conditions prévues à l'article 22 de la convention collective, en prenant pour acquis l'ancienneté que le salarié aurait obtenue à 65 ans, quel que soit son âge lors de sa mise à la retraite entre 60 et 65 ans.

L'employeur qui désire mettre un salarié à la retraite devra lui notifier son intention de préférence par lettre recommandée avec accusé de réception, en respectant un préavis de 4 mois. Le contrat de travail prendra fin dans tous les cas à la fin d'un mois civil.

Si l'employeur ne procède pas à cette notification, le contrat de travail se poursuit jusqu'à ce que cette notification soit effectuée avec le même préavis que celui fixé à l'alinéa précédent.

2. Départ à la retraite

Le salarié peut quitter volontairement l'entreprise pour bénéficier de son droit à la retraite. Il doit alors respecter le préavis suivant :

- 1 mois s'il a entre 6 mois et 2 ans d'ancienneté ;
- 2 mois, s'il a au moins 2 ans d'ancienneté.

3. La résiliation du contrat de travail, à l'initiative de l'employeur ou du salarié dans les conditions fixées aux paragraphes 1 et 2 ci-dessus, ne donne pas lieu à attribution d'heures d'absences pour recherche d'emploi.

# Régime de retraite

# Article 21

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 19 du 27 mars 1997 en vigueur le 1er jour du mois civil suivant la publication de l'arrêté d'extension BO conventions collectives 97-19 étendu par arrêté du 19 juillet 1999 JORF 30 juillet 1999.

1. Régime de retraite complémentaire ETAM et cadres

Les employeurs doivent obligatoirement adhérer pour leurs salariés à une caisse de retraite affiliée à l'ARRCO.

La cotisation portera sur la totalité des appointements pour les ETAM dans la limite du plafond fixé par l'ARRCO et sur la fraction de salaire inférieure ou égale au plafond d'assurance vieillesse de la sécurité sociale pour les cadres. Le taux contractuel ne pourra être inférieur au taux minimum fixé par l'ARRCO.

Conformément à la délibération AGIRC du 16 juin 1988 annexée à la présente convention collective, les ETAM inscrits aux articles 4 bis et 36 du régime de retraite et de prévoyance des cadres institué par la convention collective nationale du 14 mars 1947 ne sont pas visés par cet article pour la part de salaire excédant le plafond de la sécurité sociale.

2. Régime de retraite des cadres

Les employeurs doivent obligatoirement adhérer à une institution de retraite de leur choix affiliée à l'AGIRC pour les ingénieurs et cadres de leur entreprise.

Le fait pour un ETAM d'être affilié à une caisse de retraite des cadres au titre des articles 4 bis et 36 n'entraîne pas l'application des clauses conventionnelles " Ingénieurs et cadres ".

# Indemnité de départ en retraite

# Article 22

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 28 du 28 avril 2004 art. 1 en vigueur à l'extension BO conventions collectives 2004-21 étendu par arrêté du 16 juillet 2004 JORF 28 juillet 2004.

ETAM et I.C. :

Une indemnité de départ en retraite est accordée au salarié dont le contrat prend fin dans les conditions prévues à l'article 20.

Le montant de cette indemnité est fixé en fonction de l'ancienneté acquise à la date du départ en retraite.

A 5 ans révolus, 1 mois, plus, à partir de la sixième année, 1/5 de mois par année d'ancienneté supplémentaire.

Le mois de rémunération s'entend, dans le cas particulier, comme le 1/12 de la rémunération des 12 derniers mois précédant la notification de la rupture du contrat de travail, le salaire mensuel étant compté sans primes ni gratifications, ni majoration pour heures supplémentaires au-delà de l'horaire normal, ni majoration de salaire ou indemnité liée à un déplacement ou à un détachement (1).

(1) Alinéa étendu sous réserve de l'application de l'article L. 122-14-13 du code du travail (arrêté du 13 avril 1988, art. 1er).

# Titre IV : Congés

# Durée du congé

# Article 23

_En vigueur étendu_

Tout salarié ETAM et I.C. ayant au moins 1 an de présence continue dans l'entreprise à la fin de la période ouvrant droit aux congés payés aura droit à 25 jours ouvrés de congés (correspondant à 30 jours ouvrables).

Il est en outre accordé en fonction de l'ancienneté acquise à la date d'ouverture des droits :

- après une période de 5 années d'ancienneté : 1 jour ouvré supplémentaire ;
- après une période de 10 années d'ancienneté : 2 jours ouvrés supplémentaires ;
- après une période de 15 années d'ancienneté : 3 jours ouvrés supplémentaires ;
- après une période de 20 années d'ancienneté : 4 jours ouvrés supplémentaires,

indépendamment de l'application des dispositions relatives aux congés pour événements familiaux.

Cette durée est formulée en jours ouvrés (lundis, mardis, mercredis, jeudis et vendredis non fériés et non
chômés).

Il est précisé que lorsque l'employeur exige qu'une partie des congés à l'exclusion de la cinquième semaine
soit prise en dehors de la période du 1er mai au 31 octobre, il sera attribué :

- 2 jours ouvrés de congés supplémentaires lorsque le nombre de jours ouvrés de congé pris en dehors de cette période est au moins égal à 5 ;

- 1 jour ouvré de congé supplémentaire lorsque le nombre de jours ouvrés de congé pris en dehors de cette période est égal à 3 ou 4 (1).

(1) Alinéa étendu sous réserve de l'application de l'article L. 223-8 du code du travail (arrêté du 13 avril 1988, art. 1er).

# Conditions d'attribution des congés

# Article 24

_En vigueur étendu_

Au cas où le salarié n'aurait pas 1 année de présence à la fin de la période ouvrant droit aux congés, il aura droit à un congé calculé pro rata temporis sur la base de 25 jours ouvrés par an.

Il pourra prendre un congé supérieur au nombre de jours payés dans la limite des jours de congés légaux, la période complémentaire n'ouvrant droit à aucune rétribution ou indemnité. Par contre l'employeur ne saurait obliger un salarié à prendre un congé non rémunéré.

Les salariés rappelés au cours de leurs congés pour motif de service auront droit, à titre de compensation, à 2 jours de congés supplémentaires et au remboursement sur justification des frais occasionnés par ce rappel.

# Période de congés

# Article 25

_En vigueur étendu_

Les droits à congé s'acquièrent du 1er juin de l'année précédente au 31 mai de l'année en cours.

La période de prise de ces congés, dans tous les cas, est de treize mois au maximum. Aucun report de congés ne peut être toléré au-delà de cette période sauf demande écrite de l'employeur.

L'employeur peut soit procéder à la fermeture totale de l'entreprise dans une période située entre le 1er mai et le 31 octobre, soit établir les congés par roulement après consultation du comité d'entreprise (ou à défaut des délégués du personnel) sur le principe de cette alternative.

Si l'entreprise ferme pour les congés, la date de fermeture doit être portée à la connaissance du personnel au plus tard le 1er mars de chaque année.

# Modalités d'application

# Article 26

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Les dates individuelles des congés seront fixées par l'employeur après consultation des intéressés et en fonction des nécessités du service. La liste de principe des tours de départ sera portée à la connaissance des intéressés 2 mois avant leur départ.

Satisfaction sera donnée dans toute la mesure compatible avec le service aux salariés dont les enfants fréquentent l'école et qui désirent prendre leur congé pendant une période de vacances scolaires.

Les conjoints travaillant dans la même entreprise ont droit à un congé simultané.

Lorsque plusieurs membres de la même famille vivant sous le même toit travaillent dans la même entreprise, le congé leur sera accordé simultanément s'ils le désirent, dans la mesure compatible avec le service.

Si l'employeur ou le salarié, sous un délai inférieur à 2 mois, exprime son désir de voir modifier les dates de congé initialement fixées, la modification ne peut intervenir qu'après accord préalable entre les deux parties. Lorsque l'entreprise prend l'initiative de cette modification, elle s'engage à verser sur justificatifs un dédommagement correspondant aux frais éventuels occasionnés.

# Périodes d'absence entrant dans le calcul de la durée des congés

# Article 27

_En vigueur étendu_

Pour le calcul de la durée du congé, sont notamment considérés comme période de travail effectif :

- la période de congé de l'année précédente ;

- les périodes de repos légal des femmes en couches et le congé d'adoption ;

- les périodes de suspension du contrat de travail par suite d'accidents du travail ou de maladies professionnelles dans la limite d'une durée ininterrompue de 1 an ;

- les périodes d'arrêt pour maladie ou accident lorsqu'elles donnent lieu à maintien du salaire en application de la convention collective ;

- les périodes militaires obligatoires ;

- les absences exceptionnelles prévues par la convention collective pour exercice du droit syndical et pour événements familiaux ;

- les périodes de stages de formation professionnelle ;

- les congés de formation économique, sociale et syndicale.

Le collaborateur absent pour l'un de ces motifs à la date prévue pour ses vacances pourra choisir entre la prise effective de congé au moment de son retour s'il a lieu avant le 31 mai et l'indemnité compensatrice correspondante.

# Indemnité de congés payés

# Article 28

_En vigueur étendu_

L'indemnité de congés est égale au dixième de la rémunération perçue par l'intéressé au cours de la période de référence, sans pouvoir être inférieure pour les IC et les ETAM à la rémunération qui aurait été perçue pendant la période de congé pour un horaire normal de travail, et pour les CE au montant de la rémunération minimum.

# Absences exceptionnelles

# Article 29

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 3 du 30 mai 1989 étendu par arrêté du 18 octobre 1989 JORF 28 octobre 1989.

ETAM et I.C. :

Des autorisations d'absences exceptionnelles non déductibles des congés et n'entraînant pas de réduction d'appointements seront accordées au salarié pour :

- se marier : 4 jours ouvrés ;
- assister aux obsèques de son conjoint ou d'un de ses enfants : 2 jours ouvrés ;
- assister au mariage d'un de ses enfants : 1 jour ouvré ;
- assister aux obsèques de ses ascendants : 2 jours ouvrés ;
- assister aux obsèques de ses collatéraux jusqu'au 2e degré (frère ou soeur) : 1 jour ouvré ;
- assister aux obsèques de son beau-père, de sa belle-mère : 1 jour ouvré.

Si le décès du conjoint ou d'un ascendant ou d'un descendant au 1er degré intervient pendant que le salarié est en déplacement en France ou à l'étranger, l'entreprise prend en charge les frais de déplacement des salariés en mission en France ou à l'étranger dans les mêmes conditions que pour un voyage de détente.

Des autorisations d'absences exceptionnelles seront également accordées aux salariés pour tests préliminaires militaires obligatoires. Toutefois, le remboursement de ces jours d'absence sera limité à 3 jours ouvrés et ne sera effectué que sur demande justifiée par la présentation de la convocation.

Les pères de famille ont droit, à l'occasion de chaque naissance ou adoption, à un congé de 3 jours ouvrés consécutifs ou non, inclus dans une période de 15 jours entourant la date de naissance ou suivant l'arrivée au foyer de l'enfant placé en vue de son adoption.

Les entreprises s'efforceront de définir des mesures permettant aux salariés de s'absenter afin de soigner un enfant malade âgé de 12 ans au plus.

CE (1) :

Des autorisations d'absences exceptionnelles, non déductibles des congés et n'entraînant pas réduction de la rémunération mensuelle garantie, seront accordées au chargé d'enquête pour :

- se marier : possibilité de ne pas exécuter les travaux proposés pendant quatre jours ouvrables ;
- assister aux obsèques de son conjoint ou d'un de ses enfants : possibilité de ne pas exécuter les travaux proposés pendant 2 jours ouvrables ;
- assister au mariage d'un de ses enfants : possibilité de ne pas exécuter les travaux proposés pendant 1 jour ouvrable ;
- assister aux obsèques de ses ascendants : possibilité de ne pas exécuter les travaux proposés pendant 2 jours ouvrables ;
- assister aux obsèques de ses collatéraux jusqu'au 2e degré : possibilité de ne pas exécuter les travaux proposés pendant 1 jour ouvrable.

Des possibilités de refuser le travail proposé pendant trois jours ouvrables seront également accordées aux chargés d'enquête pour tests de présélection militaire obligatoires, sur demande justifiée par la présentation de la convocation.

(1) Paragraphe étendu sous réserve de l’application de l’article L. 226-1, alinéa 2, du code du travail (arrêté du 13 avril 1988, art. 1er).

# Congé sans solde

# Article 30

_En vigueur étendu_

Un congé sans solde peut être accordé par l'employeur, sur la demande de l'intéressé.

Les modalités d'application et de fin de ce congé doivent faire l'objet d'une notification écrite préalable.

Le congé sans solde entraîne la suspension des effets du contrat de travail et de ceux de la présente convention collective à l'égard de l'intéressé.

A condition de respecter les modalités prévues ci-dessus, notamment pour la reprise du travail, l'intéressé, à l'expiration de ce congé, retrouve ses droits et ses avantages acquis antérieurement. Toutefois, si les nécessités de bon fonctionnement obligent l'employeur à licencier un salarié pendant une suspension de contrat de travail, il devra aviser l'intéressé de sa décision suivant la procédure légale et lui verser le montant des indemnités prévues à l'article 19.

# Prime de vacances

# Article 31

_En vigueur étendu_

L'ensemble des salariés bénéficie d'une prime de vacances d'un montant au moins égal à 10 % de la masse globale des indemnités de congés payés prévus par la convention collective de l'ensemble des salariés.

Toutes primes ou gratifications versées en cours d'année à divers titres et quelle qu'en soit la nature peuvent être considérées comme primes de vacances à condition qu'elles soient au moins égales aux 10 % prévus à l'alinéa précédent et qu'une partie soit versée pendant la période située entre le 1er mai et le 31 octobre.

# Titre V : Rémunération et aménagement du temps de travail

# Généralités

# Article 32

_En vigueur étendu_

ETAM hors CE

La rémunération normale est basée sur des appointements mensuels calculés sur l'horaire légal, majorés ou minorés suivant que l'horaire normal de l'entreprise est supérieur ou inférieur à l'horaire légal.

Les appointements minimaux relatifs à chaque emploi des ETAM sont déterminés par l'application aux coefficients hiérarchiques des valeurs du point de rémunération.

Les valeurs du point de rémunération seront examinés deux fois par an par la commission paritaire.

Dans les barèmes des appointements minimaux garantis afférents aux positions définies, sont inclus les avantages en nature évalués d'un commun accord et mentionnés dans la lettre d'engagement ainsi que les rémunérations accessoires en espèces, mensuelles ou non, fixées par la lettre d'engagement (ou par la lettre de régularisation d'engagement ou par un accord ou une décision ultérieure).

Pour établir si l'ETAM reçoit au moins le minimum le concernant, les avantages prévus ci-dessus doivent être intégrés dans la rémunération annuelle dont 1/12 ne doit, en aucun cas, être inférieur à ce minimum.

Par contre, les primes d'assiduité et d'intéressement, si elles sont pratiquées dans l'entreprise, les primes et gratifications de caractère exceptionnel et non garanties ne sont pas comprises dans le calcul des appointements minimaux, non plus que les remboursements de frais, les indemnités en cas de déplacement ou détachement, la rémunération des heures supplémentaires.

Rémunération CE

Le calcul de la rémunération des chargés d'enquête est basé :

- d'une part sur une grille prévoyant une rémunération minimale au questionnaire variable suivant le type d'enquête ou sur tout autre système donnant des résultats équivalents. Cette grille, établie en fonction de la valeur du point de rémunération, est donnée en annexe ;

- d'autre part, pour les travaux annexes à l'enquête (entraînement, discussion après enquête, etc.) sur le coefficient hiérarchique correspondant à la classification du chargé d'enquête.

Les chargés d'enquête sont assurés d'une rémunération mensuelle minimum garantie.

Pour ceux dont il est convenu qu'ils doivent être disponibles à plein temps, la garantie mensuelle ne pourra être inférieure au produit résultant de l'application du coefficient 230 et de la valeur du point.

Pour ceux dont il est convenu qu'ils ne doivent être disponibles que partiellement, cette garantie sera réduite d'un commun accord toute proportion gardée.

Les chargés d'enquête reçoivent mensuellement le montant du salaire correspondant aux travaux effectivement réalisés. Seuls sont payés les travaux réellement effectués et reconnus valables après contrôle, le délai pour effectuer le contrôle ne pouvant excéder un mois à compter de la réception par l'institut des derniers questionnaires de l'enquête.

Dans le cas d'une suspension ou d'une rupture de contrat en cours d'année, sauf pour raison de maladie dans les limites fixées par l'article 41, cette garantie mensuelle est réduite au prorata du temps d'indisponibilité.

En cas de licenciement pour faute grave, la garantie mensuelle est réduite au prorata du temps de validité du contrat antérieur à la faute.

Les barèmes de rémunération, résultant de l'application de la grille, incluent conventionnellement les majorations pour dépassement d'horaire au-delà de l'horaire hebdomadaire légal ainsi que la rémunération des jours fériés chômés.

L'employeur peut confier mensuellement aux chargés d'enquête des travaux que ceux-ci sont tenus d'accepter à concurrence d'un volume de rémunération égal à 110 % de la rémunération mensuelle garantie.

Une régularisation des comptes sera effectuée chaque trimestre.

En cas de refus, le montant de la garantie mensuelle est réduit d'autant.

Lorsqu'il apparaîtra, lors de la régularisation trimestrielle de la rémunération d'un chargé d'enquête, que le total de ses activités rémunérées dépasse 330 % de la garantie mensuelle, il se verra attribuer en compensation un complément de rémunération dont le montant sera fixé par convention dans chaque institut.

Toutefois, les activités mensuelles dépassant 110 % de la garantie mensuelle qui auraient déjà fait l'objet d'une majoration n'entreront pas en ligne de compte pour déterminer cet éventuel dépassement.

La grille des rémunérations en annexe donne les rémunérations minimales garanties des chargés d'enquête en fonction des divers types d'enquêtes qu'ils peuvent être amenés à réaliser.

La durée moyenne d'interview est la durée de passation du questionnaire. La rémunération figurant en regard tient compte à la fois du temps moyen réel nécessaire au recueil de l'information, du temps moyen de recherche et de mise au net du questionnaire.

Caractère forfaitaire de la rémunération des ingénieurs et cadres

Etant donné le rôle dévolu aux ingénieurs et cadres, il est fréquent que leurs heures de présence ne puissent être fixées d'une façon rigide ; elles correspondent aux nécessités de l'organisation du travail et de la surveillance de son exécution.

Les appointements minimaux découlent des coefficients et des valeurs du point et correspondent à l'horaire légal de références.

Les valeurs du point seront fixées aux mêmes dates que pour celles des ETAM.

Les appointements des IC ont un caractère forfaitaire. Ce forfait, dans le cadre de l'horaire normal de l'entreprise, correspond aux conditions réelles de travail de l'IC et englobe notamment les heures supplémentaires occasionnelles de l'IC et, le cas échéant, l'adaptation aux horaires habituels des clients avec lesquels ils travaillent.

Ce forfait devra être révisé si les conditions réelles de travail de l'IC entraînaient de façon permanente une diminution ou une augmentation de son temps de travail.

Dans l'horaire imposé aux IC, il sera tenu compte, en tout état de cause, de la nécessité d'un repos hebdomadaire normal. Cette obligation se traduira, le cas échéant, par l'octroi de repos compensateurs.

Dans les barèmes des appointements minimaux garantis afférents aux positions définies, sont inclus les avantages en nature évalués d'un commun accord et mentionnés dans la lettre d'engagement ainsi que les rémunérations accessoires en espèces, mensuelles ou non, fixées par la lettre d'engagement (ou par la lettre de régularisation d'engagement ou par un accord ou une décision ultérieure).

Pour établir si l'ingénieur ou cadre reçoit au moins le minimum le concernant, les avantages prévus au paragraphe ci-dessus doivent être intégrés dans la rémunération annuelle dont 1/12 ne doit, en aucun cas, être inférieur à ce minimum.

Par contre, les primes d'assiduité et d'intéressement, si elles sont pratiquées dans l'entreprise, les primes et gratifications de caractère exceptionnel et non garanties ne sont pas comprises dans le calcul des appointements minimaux non plus que les remboursements de frais, et les indemnités en cas de déplacement ou de détachement.

# Heures supplémentaires

# Article 33

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

ETAM hors CE :

A.- Rémunération des heures supplémentaires :

Les heures supplémentaires de travail contrôlées, effectuées par le personnel ETAM, sont payées avec les majorations légales.

Des repos compensateurs seront attribués conformément aux dispositions légales.

B. - Contingent annuel :

Il est prévu un contingent annuel de 130 heures supplémentaires utilisables sans autorisation de l'inspecteur du travail.

# Modulation indicative annuelle de la durée du travail (1)

# Article 34

_En vigueur étendu_

Dernière modification : Remplacé par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Il est possible de prévoir par accord d'entreprise une modulation indicative annuelle de la durée du travail, avec révision tous les 3 mois, dans une amplitude de plus ou moins 2 heures autour de 39 heures hebdomadaires, le salaire restant constant.

Le comité d'entreprise, ou, à défaut, les délégués du personnel, est consulté sur cette modulation.

(1) Dispositions dérogatoires.

# Travail exceptionnel de nuit, du dimanche et des jours fériés

# Article 35 (1)

_En vigueur étendu_

Dernière modification : Modifié par Avenant du 28 avril 2004 BO conventions collectives 2004-27 étendu par arrêté du 26 octobre 2004 JORF 9 novembre 2004.

(1) Voir aussi avenant du 28 avril 2004 relatif aux dispositions financières du travail du dimanche et des jours fériés, non étendu.

# Dispositions communes

# Article 35.1

## Dispositions communes

_En vigueur étendu_

Le travail du dimanche et des jours fériés est subordonné aux dispositions de la législation du travail, et spécifiquement au titre II du code du travail portant sur les repos et congés. Par conséquent, lorsqu'une société est amenée à exercer des travaux non dérogatoires au repos dominical, elle doit faire la demande auprès du préfet du département et reste, en outre, tenue de respecter les dispositions légales (1).

Le nombre de dérogations est limité par la présente convention collective à 15 autorisations par année et par salarié.

(2) Phrase exclue de l'extension (arrêté du 26 octobre 2004, art. 1er).

# Travail exceptionnel du dimanche et des jours fériés ETAM

# Article 35.2

_En vigueur étendu_

Dans les entreprises entrant dans le champ professionnel d'application de la présente convention collective nationale à l'exception de celles relevant des codes NAF 748 J, 923 D et 703 D, auxquelles s'applique l'accord national du 5 juillet 2001, les heures ainsi effectuées sont rémunérées avec une majoration de 100 %, indépendamment des majorations résultant des heures supplémentaires éventuelles.

# Travail exceptionnel du dimanche et des jours fériés IC

# Article 35.3

_En vigueur étendu_

Dans les entreprises entrant dans le champ professionnel d'application de la présente convention collective nationale à l'exception de celles relevant des codes NAF 748 J, 923 D et 703 D, auxquelles s'applique l'accord national du 5 juillet 2001, et uniquement pour les salariés dont le temps de travail est décompté selon les modalités "standard" et "réalisation de missions" au sens du chapitre II, articles 2 et 3, de l'accord national du 22 juin 1999 sur la durée du travail, les heures ainsi effectuées sont rémunérées avec une majoration de 100 %, indépendamment des majorations résultant des heures supplémentaires éventuelles pour les salariés dont le décompte du temps de travail est en heures, ou des TEA pour les salariés bénéficiant d'une convention de forfait hebdomadaire en heures.

CE :

Si par suite de circonstances exceptionnelles et à la demande expresse de l'employeur, un chargé d'enquête est appelé à travailler soit de nuit (entre 22 heures et 6 heures), soit un dimanche, soit un jour férié, les travaux effectués sont rémunérés avec une majoration de 50 % par rapport aux rémunérations prévues par la grille des rémunérations minimales.

Le travail du dimanche ainsi que le travail de nuit des femmes sont subordonnés aux dispositions légales.

Si par suite des variations dans le volume et les conditions d'exécution du travail, qui sont inhérentes à l'activité d'enquête, un chargé d'enquête est amené à travailler à une heure quelconque comprise entre 6 heures et 22 heures, il n'en résulte aucune modification de la rémunération.

# Travail habituel de nuit, du dimanche et des jours fériés

# Article 36

_En vigueur étendu_

### ETAM :

Définition du travail de nuit :

Est considéré comme travail de nuit, conformément aux dispositions légales, tout travail ayant lieu entre 22 heures et 5 heures.

Toutefois, conformément à ces mêmes dispositions légales, toute autre période de 7 heures consécutives, comprise entre 22 heures et 7 heures peut être substituée à la période prévue ci-dessus. L'utilisation de cette possibilité est subordonnée à la conclusion d'un accord d'entreprise, ou à l'autorisation de l'inspecteur du travail donnée après consultation des délégués syndicaux et avis du comité d'entreprise ou des délégués du personnel s'ils existent.

Travail de nuit des femmes :

Conformément aux dispositions légales, les femmes ne peuvent être employées à aucun travail de nuit, sauf celles qui occupent des postes de direction ou de caractère technique et impliquant une responsabilité.

Le repos de nuit des femmes doit avoir une durée de 11 heures consécutives au minimum.

Travail du dimanche :

Le travail du dimanche est subordonné aux dispositions de la législation du travail.

Lorsqu'une société désire bénéficier de l'une des exceptions à l'attribution du repos hebdomadaire le dimanche, elle doit en faire la demande auprès du préfet du département.

# ETAM : paiement habituel du travail de nuit, du dimanche et des jours fériés

# Article 37

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Lorsque l'organisation du travail nécessite le travail habituel de nuit, du dimanche ou des jours fériés, les heures de travail ainsi effectuées bénéficient d'une majoration de 25 % appliquée sur le taux horaire découlant du minimum hiérarchique, sous réserve que ces heures soient incluses dans un poste comportant au moins 6 heures consécutives.

Pour apprécier si cette majoration est perçue par l'intéressé, il est tenu compte des avantages particuliers déjà accordés à ce titre dans l'entreprise et soumis à cotisations sociales.

# ETAM et IC : équipes de suppléance

# Article 38

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Si l'organisation du travail le rend nécessaire, des équipes de suppléance pourront être mises en place pendant les jours de repos en fin de semaine du reste du personnel conformément aux dispositions légales.

# Classifications

# Article 39

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

## Classification des employés, techniciens et agents de maîtrise :

Les classifications des employés, techniciens et agents de maîtrise figurent en annexe I de la  convention. Dans les entreprises qui ont des agents de maîtrise, ceux-ci sont classés dans le groupe 3 de  grille ETAM

Ces classifications s'imposent à toutes les entreprises soumises à la convention. Toute difficulté d'application tenant à l'activité de l'entreprise peut faire l'objet d'un accord paritaire d'entreprise, mais sous réserve de l'accord de la commission paritaire d'interprétation de la convention.

a) La fonction remplie par l' ETAM est seule prise en considération pour son classement dans les emplois prévus par la classification en cause.

b) L'ETAM dont les fonctions relèvent de façon continue de diverses catégories est considéré comme appartenant à la catégorie la plus élevée parmi celles-ci.

## Classification des chargés d'enquête :

Compte tenu de la nature même des travaux d'enquête, les chargés d'enquête ont une même classification et un même coefficient : 230.

La valeur du point est celle fixée pour les ETAM.

## Classification des ingénieurs et cadres :

Les classifications des ingénieurs et cadres figurent en annexe II de la présente convention.

La classification des cadres sera effectuée en tenant compte des responsabilités assumées et des connaissances mises en application.

Ces classifications s'imposent à toutes les entreprises soumises à la convention. Toute difficulté d'application tenant à l'activité de l'entreprise peut faire l'objet d'un accord de la commission paritaire d'interprétation de la convention.

a) La fonction remplie par l'ingénieur ou cadre est seule prise en considération pour son classement dans les emplois prévus par la classification en cause.

b) L'ingénieur ou cadre dont les fonctions relèvent de façon continue de diverses catégories est considéré comme appartenant à la catégorie la plus élevée parmi celles-ci.

# Bulletin de paie

# Article 40

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Le bulletin de paie devra comporter les mentions légales et notamment :

- le nom ou la raison sociale, et l'adresse de l'employeur ;

- le numéro SIRET de l'établissement ;

- son numéro de code APE ;

- la référence de l'organisme auquel l'employeur verse les cotisations de sécurité sociale ainsi que le numéro d'immatriculation sous lequel ces cotisations sont versées ;

- le nom et le prénom de la personne à qui est délivré le bulletin de paie et l'emploi qu'elle occupe ;

- la classification professionnelle du salarié et le coefficient hiérarchique correspondant ;

- la période à laquelle se rapporte la rémunération versée ;

- le montant des appointements mensuels de base ou le montant des appointements forfaitaires.

- l'intitulé de la convention collective applicable ;

- la mention incitant le salarié à conserver son bulletin de paie sans limitation de durée.

# Titre VI : Maladie - Accidents

# Absences maladie

# Article 41

_En vigueur étendu_

a) Les absences justifiées par l'incapacité temporaire de travail résultant de maladie ou d'accident dûment constaté par certificat médical, et notifiées ainsi qu'il est dit à l'article 42 ci-après, ne constituent pas une cause de rupture du contrat de travail, mais une suspension de celui-ci.

b) Si les nécessités de bon fonctionnement obligent l'employeur à licencier un salarié absent pour incapacité de travail constatée par certificat médical, l'employeur devra respecter les procédures prévues à cet effet.

Les appointements, ou pour les CE le bénéfice de la rémunération des 12 derniers mois, seront maintenus à l'intéressé tant qu'il sera malade, dans les limites prévues ci-après à l'article 43 ETAM et à l'article 43 IC.

A la fin de la période d'indemnisation, ou au moment du rétablissement du salarié, si celui-ci a lieu avant que le salarié ait épuisé les droits qu'il tient des stipulations qui suivent, il lui sera payé, sous réserve du préavis normal, l'indemnité de licenciement à laquelle il aurait droit en vertu des dispositions de la présente convention.

# Formalités

# Article 42

_En vigueur étendu_

Dès que possible, et au plus tard dans les 24 heures, le salarié doit avertir son employeur du motif de la durée probable de son absence.

Cet avis est confirmé dans le délai maximal de 48 heures à compter du premier jour de l'indisponibilité, prévu par la législation de la sécurité sociale, au moyen d'un certificat médical délivré par le médecin traitant du salarié. Lorsqu'il assure un complément d'allocations maladie aux indemnités journalières de la sécurité sociale, l'employeur a la faculté de faire contre-visiter le salarié par un médecin de son choix.

# Incapacité temporaire de travail

# Article 43

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 19 du 27 mars 1997 en vigueur le 1er jour du mois civil suivant la publication de l'arrêté d'extension BO conventions collectives 97-19, *étendu avec exclusions par arrêté du 19 juillet 1999 JORF 30 juillet 1999*.

### ETAM :

En cas de maladie ou d'accident dûment constatés par certificat médical et contre-visite, s'il y a lieu, les ETAM recevront les allocations maladie nécessaires pour compléter, jusqu'à concurrence des appointements ou fractions d'appointements fixées ci-dessous, les sommes qu'ils percevront à titre d'indemnité, d'une part, en application des lois sur les accidents du travail et les maladies professionnelles et des lois sur l'assurance maladie, d'autre part, en compensation de perte de salaire d'un tiers responsable d'un accident (1).

Les indemnités versées par un régime de prévoyance auquel aurait fait appel l'employeur viendront également en déduction.

Dans le cas d'incapacité par suite d'accident du travail ou de maladie professionnelle survenus au service de l'employeur, les allocations prévues ci-dessous sont garanties dès le premier jour de présence, alors que dans les autres cas de maladie ou d'accident elles ne sont acquises qu'après 1 an d'ancienneté.

Dans les autres cas de maladie ou d'accident :

- pour l'ETAM ayant plus de 1 an d'ancienneté et moins de 5 ans : 1 mois à 100 % d'appointements bruts ; les 2 mois suivants : 80 % de ses appointements bruts ;
- pour l'ETAM ayant plus de 5 ans d'ancienneté : 2 mois à 100 % d'appointements bruts ; le mois suivant : 80 % de ses appointements bruts.

Il est précisé que l'employeur ne devra verser que les sommes nécessaires pour compléter ce que verse la sécurité sociale et, le cas échéant, un régime de prévoyance, ainsi que les compensations de perte de salaire d'un tiers responsable (1) , jusqu'à concurrence de ce qu'aurait perçu, net de toute charge, l'ETAM malade ou accidenté s'il avait travaillé à temps plein ou à temps partiel, non compris primes et gratifications (2).

Si l'ancienneté fixée par l'un quelconque des alinéas précédents est atteinte par l'ETAM au cours de sa maladie, il recevra, à partir du moment où cette ancienneté sera atteinte, l'allocation ou la fraction d'allocation fixée par la nouvelle ancienneté pour chacun des mois de maladie restant à courir.

Le maintien du salaire s'entend dès le premier jour d'absence pour maladie ou accident dûment constatés par certificat médical.

Les allocations fixées ci-dessus constituent le maximum auquel l'ETAM aura droit pour toute période de 12 mois consécutifs au cours de laquelle il aura eu une ou plusieurs absences pour maladie ou accident.

Pour les incapacités temporaires de travail supérieures à 90 jours consécutifs le relais des garanties sera assuré aux conditions prévues par l'accord prévoyance annexé à la présente convention collective.

### IC :

En cas de maladie ou d'accident dûment constatés par certificat médical et contre-visite, s'il y a lieu, les IC recevront les allocations maladie nécessaires pour compléter, jusqu'à concurrence des appointements ou fractions d'appointements fixées ci-dessous, les sommes qu'ils percevront à titre d'indemnité, d'une part, en application des lois sur les accidents du travail et les maladies professionnelles et des lois sur l'assurance maladie, d'autre part, en compensation de perte de salaire d'un tiers responsable d'un accident (1).

Les indemnités versées par un régime de prévoyance auquel aurait fait appel l'employeur viendront également en déduction.

Dans le cas d'incapacité par suite d'accident du travail ou de maladie professionnelle survenus au service de l'employeur, les allocations prévues ci-dessous sont garanties dès le premier jour de présence, alors que dans les autres cas de maladie ou d'accident elles ne sont acquises qu'après 1 an d'ancienneté.

Cette garantie est fixée à 3 mois entiers d'appointements.

Il est précisé que l'employeur ne devra verser que les sommes nécessaires pour compléter ce que verse la sécurité sociale et, le cas échéant, un régime de prévoyance, ainsi que les compensations de perte de salaire d'un tiers responsable (1) , jusqu'à concurrence de ce qu'aurait perçu, net de toute charge, l'IC malade ou accidenté s'il avait travaillé à temps plein ou à temps partiel, non compris primes et gratifications (2).

Si l'ancienneté de 1 an est atteinte par l'IC au cours de sa maladie, il recevra à partir du moment où l'ancienneté sera atteinte, l'allocation fixée par le présent article pour chacun des mois de maladie restant à courir.

Le maintien du salaire s'entend dès le premier jour d'absence pour maladie ou accident dûment constatés par certificat médical.

Les allocations fixées ci-dessus constituent le maximum auquel l'IC aura droit pour toute période de 12 mois consécutifs au cours de laquelle il aura eu une ou plusieurs absences pour maladie ou accident.

Pour les incapacités temporaires de travail supérieures à 90 jours consécutifs, le relais des garanties sera assuré aux conditions prévues par l'accord "Prévoyance" annexé à la présente convention collective. (1) Termes exclus de l'extension (arrêté du 19 juillet 1999, art. 1er).

(2) Alinéa étendu sous réserve des dispositions de la loi n° 78-49 du 19 janvier 1978 (art. 7 de l'accord national interprofessionnel du 10 décembre 1977 annexé) (arrêté du 19 juillet 1999, art. 1er).

# Maternité

# Article 44

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Les collaboratrices ayant plus d'un an d'ancienneté dans l'entreprise à la date de leur arrêt de travail pour maternité conserveront le maintien intégral de leurs appointements mensuels pendant la durée du congé légal sous déduction des indemnités versées par la sécurité sociale et les régimes de prévoyance.

A partir du troisième mois de leur grossesse, les femmes enceintes bénéficieront d'une réduction d'horaire rémunérée de 20 minutes par jour.

Lorsque les consultations prénatales obligatoires auront lieu pendant les heures de travail, le temps de travail ainsi perdu sera payé aux intéressées, qui devront prévenir leur employeur en temps utile.

Les femmes désirant élever leur enfant auront droit à un congé sans solde dans le cadre de la législation en vigueur (1).

(1) Alinéa étendu sous réserve de l'application des articles L. 122-28-1 et suivants du code du travail (arrêté du 13 avril 1988, art. 1er).

# Décès

# Article 45

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 19 du 27 mars 1997 en vigueur le 1er jour du mois civil suivant la publication de l'arrêté d'extension BO conventions collectives 97-19 étendu par arrêté du 19 juillet 1999 JORF 30 juillet 1999.

Les dispositions relatives à l'assurance décès sont prévues par l'accord "Prévoyance" du 27 mars 1997 annexé à la présente convention collective.

# Titre VII : Formation (modifié par l'accord national du 27 décembre 2004 sur la formation professionnelle)

# Formation professionnelle

# Article 46

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 31 du 31 mars 2005 art. 2 en vigueur à l'extension BO conventions collectives 2005-19 étendu par arrêté du 3 octobre 2005 JORF 13 octobre 2005.

(article supprimé par l'avenant n° 31 du 31 mars 2005).

# Congé de formation

# Article 47

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 31 du 31 mars 2005 art. 2 en vigueur à l'extension BO conventions collectives 2005-19 étendu par arrêté du 3 octobre 2005 JORF 13 octobre 2005.

(article supprimé par l'avenant n° 31 du 31 mars 2005).

# Formation et information du personnel d'encadrement

# Article 48

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 31 du 31 mars 2005 art. 2 en vigueur à l'extension BO conventions collectives 2005-19 étendu par arrêté du 3 octobre 2005 JORF 13 octobre 2005.

(article supprimé par l'avenant n° 31 du 31 mars 2005).

# Organisme paritaire collecteur agréé (OPCA)

# Article 49

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 31 du 31 mars 2005 art. 2 en vigueur à l'extension BO conventions collectives 2005-19 étendu par arrêté du 3 octobre 2005 JORF 13 octobre 2005.

Conformément aux dispositions du titre VI du livre IX du code du travail et des textes réglementaires y afférents, il est créé un organisme paritaire collecteur agréé (OPCA) dénommé FAFIEC.

La gestion de cet organisme paritaire collecteur agréé (OPCA) est assurée paritairement à raison de 10 administrateurs pour les organisations patronales signataires de cet organisme paritaire collecteur agréé et de 2 administrateurs par organisation syndicale de salariés signataires de cet organisme paritaire collecteur agréé.

Toutes les entreprises comprises dans le champ d'application de la présente convention collective versent obligatoirement à l'organisme paritaire collecteur agréé les contributions obligatoires définies au titre VIII de l'accord national du 27 décembre 2004 sur la formation professionnelle, conclu dans le cadre de la convention collective nationale du 15 décembre 1987.

L'accord constitutif et le règlement intérieur de cet organisme paritaire collecteur agréé sont déterminés paritairement.

# Titre VIII : Déplacements et changements de résidence en France métropolitaine (Corse comprise)

# Frais de déplacement

# Article 50

_En vigueur étendu_

Les déplacements hors du lieu de travail habituel nécessités par le service ne doivent pas être pour le salarié l'occasion d'une charge supplémentaire ou d'une diminution de salaire.

L'importance des frais dépend du lieu où s'effectuent les déplacements, ils ne sauraient être fixés d'une façon uniforme. Ils seront remboursés de manière à couvrir les frais d'hôtel et de restaurant du salarié. Ils pourront faire l'objet d'un forfait préalablement au départ, soit par accord particulier, soit par règlement spécifique approprié.

En ce qui concerne les chargés d'enquête, s'il résulte d'un transfert de la résidence d'un chargé d'enquête un accroissement systématique de frais de déplacement nécessités par le service, ces frais supplémentaires restent entièrement à la charge du chargé d'enquête, sauf accord de l'employeur pour les prendre à sa charge.

# Ordre de mission

# Article 51

_En vigueur étendu_

Avant l'envoi d'un salarié en déplacement, un ordre de mission sera normalement établi, se référant au présent titre.

L'ordre de mission pourra être permanent pour les salariés dont les fonctions, telles que précisées dans le contrat de travail, les conduisent à effectuer des déplacements multiples pour des interventions ponctuelles.

En ce qui concerne les CE, les instructions qui précisent les conditions d'exécution de chaque enquête constituent l'ordre de mission préalable à l'exécution de chaque enquête.

# Voyage de détente

# Article 52

_En vigueur étendu_

Pendant les déplacements occasionnels de longue durée (au moins 1 mois consécutif) il sera accordé, à titre de détente, au salarié éloigné de sa famille (conjoint, enfant), un certain nombre de voyages aller et retour, dont les conditions de fréquence, de durée d'absence, de mode de locomotion devront être précisées dans l'ordre de mission ou fixées par un règlement spécifique approprié.

Ces voyages seront effectués, en principe, pendant les jours non ouvrés.

Toutefois, dans le cas où la durée du trajet serait telle que le salarié ne pourrait pas, même en voyageant de nuit, disposer de :

- 24 heures complètes dans sa famille, s'il s'agit d'un voyage hebdomadaire ;
- 48 heures s'il s'agit d'un voyage qui a lieu tous les mois,

il pourra prolonger son séjour sans qu'il soit effectué de retenue sur ses appointements, de manière à lui permettre de disposer de 24 ou 48 heures.

Le paiement de ces frais de voyage est dû, que le salarié se rende dans sa famille ou que celle-ci se rende auprès de lui, mais, dans ce dernier cas, la somme allouée ne pourra dépasser celle qui lui aurait été due pour se rendre lui-même à son domicile.

# Indemnité pour déplacement continu

# Article 53

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Le salarié dont la lettre d'engagement mentionne qu'il doit travailler tout ou partie de l'année en déplacement continu aura droit, outre son salaire, à une indemnité de remboursement de frais pendant la durée de ce déplacement.

Cette indemnité sera :

- soit forfaitaire, auquel cas, elle représentera la différence entre les frais de séjour et les dépenses normales du salarié s'il vivait au lieu où il a été engagé, et sera fixée par accord préalable entre l'employeur et le salarié, sauf règlement spécifique conformément à l'article 50 ;

- soit versée sur pièces justificatives.

# Elections

# Article 54

_En vigueur étendu_

A la demande de l'intéressé, sauf s'il y a possibilité de vote par correspondance ou par procuration, une autorisation d'absence sera accordée pour participer aux élections pour lesquelles les électeurs sont convoqués légalement et pour celles des comités d'entreprise et délégués.

Le voyage sera payé et comptera comme voyage de détente.

# Cas de suspension du remboursement des frais de déplacement

# Article 55

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Les frais de déplacement, du fait qu'ils ne constituent pas une rémunération, mais un remboursement de dépenses, ne seront pas payés pendant les vacances, les séjours de détente, les absences pour élections, convenances personnelles, périodes militaires, maladies ayant donné lieu à rapatriement ou hospitalisation. Toutefois, les frais (locations, par exemple) qui continueraient à courir pendant les absences de courte durée pourront être remboursés après accord préalable avec l'employeur.

# Détente en fin de déplacement

# Article 56

_En vigueur étendu_

Le voyage de détente, sauf lorsqu'il s'agit de participer aux élections conformément aux conditions de l'article 54, ne peut être exigé lorsqu'il se place dans les 10 derniers jours de la fin d'une mission ou d'un déplacement. Dans ce cas, un repos égal à la durée de l'absence non utilisée est accordé au salarié au retour à son point d'attache.

# Congé annuel en cours de déplacement

# Article 57

_En vigueur étendu_

Lorsqu'un salarié amené à prendre son congé annuel au cours d'une période où il se trouve en déplacement désire regagner sa résidence habituelle avant son départ en congé, ce voyage comptera comme voyage de détente au sens de l'article 52.

La nouvelle période ouvrant droit à un voyage de détente partira du jour du retour du congé.

# Maladie, accident ou décès en cours de déplacement

# Article 58

_En vigueur étendu_

En cours de déplacement, en cas de maladie ou d'accident graves ou de décès d'un salarié, les dispositions à prendre seront examinées individuellement, étant entendu qu'en cas d'hospitalisation, le salarié n'aura pas à supporter personnellement de charges supplémentaires à celles qui lui incomberaient normalement. L'entreprise donnera toutes facilités, notamment pour le remboursement des frais de transport, à un membre de la famille du salarié ou toute autre personne désignée par lui pour se rendre auprès de ce dernier.

# Moyens de transport

# Article 59

_En vigueur étendu_

Les déplacements professionnels peuvent être effectués par :

1. Tous les moyens de transport en commun selon les modalités suivantes, sauf stipulation contraire :
- avion (classe touriste) ;

- train et bateau : 2e classe ou confort équivalent pour les ETAM, 1re classe ou confort équivalent pour les IC.

2. Tous les moyens personnels du salarié lorsque celui-ci a été autorisé par son employeur à les utiliser à des fins professionnelles.

# Utilisation d'un véhicule personnel

# Article 60

_En vigueur étendu_

Lorsque le salarié utilise pour les besoins du service un véhicule automobile, une motocyclette ou un cyclomoteur, les frais occasionnés sont à la charge de l'employeur, à condition qu'un accord écrit ait précédé cette utilisation. Cet accord peut être permanent.

Le remboursement de ces frais tiendra compte de l'amortissement du véhicule, des frais de garage, de réparations et d'entretien des frais d'assurances et, éventuellement, des impôts et taxes sur le véhicule.

Le salarié devra être possesseur des documents nécessaires à la conduite du véhicule utilisé, et être régulièrement couvert par une assurance garantissant sans limitation le risque de responsabilité civile "affaires déplacements professionnels" et notamment de responsabilité civile de son employeur, en cas d'accident causé aux tiers du fait de l'utilisation de ce véhicule pour les besoins du service.

La communication de ces pièces (carte grise, permis de conduire, assurance, vignette) vaut engagement de la part du salarié de rester en règle à ce sujet, toute modification ultérieure devant être immédiatement signalée à l'employeur.

Tout manquement à cette obligation dégage la responsabilité de l'employeur.

# Changement de résidence

# Article 61

_En vigueur étendu_

Constatant l'intérêt économique et social de la mobilité géographique des salariés entrant dans le champ d'application de la présente convention, mais conscientes des répercussions qu'elle peut avoir, les parties signataires recommandent que cette mobilité ne soit pas, pour les salariés, l'occasion d'une charge supplémentaire et qu'il soit tenu compte dans toute la mesure du possible de leur situation familiale.

Le changement de résidence doit correspondre à des besoins réels de l'entreprise.

La faculté de prévoir dans le contrat de travail la possibilité d'un changement de résidence ne doit pas donner lieu à une application qui dénaturerait l'usage pour lequel elle a été prévue ; ce serait aller au-delà de l'intention des signataires que de prévoir systématiquement une clause de changement de résidence dans le contrat de travail du personnel administratif non cadre.

Toute modification du lieu de travail comprenant un changement de résidence fixe qui n'est pas acceptée par le salarié est considérée, à défaut de solution de compromis, comme un licenciement et réglée comme tel.
Dans ce cas, à la demande du salarié, une lettre constatant le motif du licenciement sera jointe au certificat de travail.

Le salarié licencié en raison de son refus de respecter la clause de mobilité figurant dans son contrat de travail se verra attribuer les indemnités légales de licenciement en remplacement des indemnités de licenciement fixées par l'article 19 de la présente convention collective.

Lorsque le salarié reçoit un ordre de changement de résidence, les frais de déménagement et de voyage occasionnés par le déplacement de sa famille (conjoint, et personnes à charge au sens de la législation fiscale) sont à la charge de l'employeur.

Le montant de ces frais est soumis à l'accord de l'employeur préalablement à leur engagement.

Les frais de déplacement de résidence, lorsque l'employeur n'a pas prévenu le salarié dans les délais suffisants pour donner congé régulier, comprennent en particulier, le remboursement du dédit éventuel à payer par le salarié à son logeur, ce dédit est en principe, égal au maximum à trois mois de loyer.

Lorsqu'un salarié recevra un ordre de changement de résidence, si les usages ou la pénurie des locaux disponibles l'amènent à louer un logement avec préavis de congé supérieur à trois mois, il devra, au préalable, obtenir l'accord de son employeur faute de quoi celui-ci ne serait tenu à lui rembourser, en cas de licenciement, que trois mois de congés.

Si un salarié est muté dans un autre lieu de travail entraînant un changement de résidence, il est considéré comme déplacé et indemnisé comme tel, tant qu'il n'aura pu installer sa famille dans sa nouvelle résidence.

En principe, cette indemnisation sera allouée pendant un an au maximum, sauf accord individuel prolongeant ce délai, en cas de nécessité.

# Licenciement après un changement de résidence

# Article 62

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

Tout salarié qui, après un changement de résidence imposé par la direction, sera licencié dans sa nouvelle résidence dans un délai de 2 ans, et sauf pour une faute grave, aura droit au remboursement des frais occasionnés par son retour et celui de sa famille au lieu de sa résidence initiale.

Le remboursement sera effectué sur présentation des pièces justificatives et ne sera dû que si le retour de l'intéressé a lieu dans un délai de 6 mois suivant notification du licenciement.

Si, dans la même hypothèse, le salarié licencié s'installe dans un autre lieu que celui de sa résidence initiale, il aura droit au remboursement des frais ci-dessus (mais également après accord avec son employeur) sauf si le salarié s'installe sur place.

En ce qui concerne l'évaluation de la limite maximale des frais qu'aurait occasionné son retour au point de départ, il doit être entendu que ces frais doivent tenir compte, s'il y a lieu, d'un accroissement de l'effectif familial ou du poids des bagages, en tenant compte également de l'accord préalable qui aura été pris entre le salarié et l'entreprise avant son déplacement.

# Décès dans la nouvelle résidence

# Article 63

_En vigueur étendu_

Dernière modification : Modifié par Avenant n° 7 du 5 juillet 1991 étendu par arrêté du 2 janvier 1992 JORF 14 janvier 1992

En cas de décès d'un salarié au lieu de sa nouvelle résidence, les frais occasionnés par le retour de sa famille (conjoint et personnes à charge) ainsi que les frais éventuels de retour du corps au lieu de résidence initiale seront à la charge de l'employeur, dans les conditions fixées aux deux articles précédents.

# Titre IX : Déplacement hors de France métropolitaine

# Conditions générales

# Article 64

_En vigueur étendu_

Les entreprises qui exercent habituellement ou occasionnellement une activité hors de France métropolitaine et qui, de ce fait, sont amenées à y envoyer certains membres de leur personnel en déplacement, doivent, à défaut de l'avoir précisé dans la lettre d'engagement, obtenir l'accord des intéressés par un avenant, à condition que la durée du déplacement soit au moins égale à trois mois continus. Cet accord prendra la forme soit d'un avenant temporaire, soit d'un nouveau contrat de travail.

Quelle que soit la formule adoptée, la lettre d'engagement ou l'avenant devra se référer aux clauses du présent titre IX de la convention collective pour autant que le personnel engagé soit soumis à la législation sociale française.

# Nature des missions

# Article 65

_En vigueur étendu_

Ces missions peuvent être de nature très variable.

A. - Sur le plan géographiqe

Les conditions de vie et de transport sont évidemment très différentes suivant qu'il s'agit d'aller dans un pays de la Communauté européenne ou dans un pays limitrophe de la France métropolitaine ou encore dans un pays d'outre-mer qui lui-même peut être tropical ou non, en voie de développement ou économiquement très développé, francophone ou non.

La convention collective ne peut donc fixer à l'avance tous les cas possibles, mais précise des règles minimales et donne la liste des paragraphes qui doivent figurer en totalité ou en partie dans l'ordre de mission.

B. - Sur le plan de la durée.

# Ordre de mission

# Article 66

_En vigueur étendu_

L'envoi en mission hors de France métropolitaine d'un salarié devra toujours, au préalable, faire l'objet d'un ordre de mission manifestant la volonté des parties sans ambiguïté et fixant les conditions spécifiques de cette mission. Cet ordre de mission constitue un avenant au contrat de travail. Les conditions d'envoi en mission peuvent faire utilement l'objet d'un accord d'entreprise ou d'une note de service.

Ces documents préciseront les cas dans lesquels les intéressés devront réclamer leur ordre de mission avant de partir. Dans certains cas l'ordre de mission peut avoir un caractère permanent.

## a) Cet ordre de mission stipulera dans tous les cas que le salarié reste rémunéré par la société d'origine ou par une filiale auprès de laquelle il se trouve détaché.

1. Les noms, prénoms, qualités et adresses des parties.

2. La nature, la durée et le lieu de la mission.

3. Les modalités d'exécution des dispositions concernant les voyages et transports.

4. La couverture des risques et des frais de voyage et de déplacement.

5. L'utilisation ou non d'un véhicule personnel, la possibilité ou non d'amener un véhicule personnel.

6. L'obligation ou non d'un contrôle médical et de vaccinations.

7. La référence, s'il y a lieu, à un accord d'entreprise relatif aux déplacements et missions.

8. Le lieu de rapatriement en fin de séjour.

9. Les éléments de rémunération, des indemnités de séjour et dépaysement, les primes éventuelles d'équipement, etc., dont les bases de calcul pourront faire l'objet de notes de service en fonction, notamment, des conditions particulières à chaque pays et de leur régime fiscal.

10. Les modalités de règlement de la rémunération, des primes et avances et incidences fiscales de ces modalités ; il devra être notamment précisé si la rémunération mensuelle et les indemnités auxquelles le salarié a droit pendant son séjour sont payables :
- soit en partie en France métropolitaine en francs français, à un compte ouvert en France au nom du salarié dans l'établissement bancaire ou postal de son choix ;

- soit en tout ou partie en monnaie locale pour sa contre-valeur au taux de change officiel.

Ces dispositions pourront être modifiées en cours de mission si les circonstances venaient à l'exiger, ou d'un commun accord entre les parties.

11. Les conditions de logement, s'il y a lieu, et d'équipement de celui-ci.

12. Les conditions dans lesquelles s'effectueront les déplacements du salarié dans le nouveau pays de résidence ;

13. Les conditions d'application des droits aux congés par dérogation au titre IV.

14. Les conditions de préavis.

15. Les conditions de la réinsertion du salarié en France à l'issue de sa mission.

## b) En outre, si la durée du déplacement est supérieure à six mois :

16. La possibilité ou non pour le salarié d'emmener sa famille.

17. Les modalités des conditions de voyage aller et retour du salarié et de sa famille (transport des personnes et des bagages).

18. Le maintien ou non des régimes de retraite et de prévoyance, du régime des Assedic, dont le salarié bénéficie en France métropolitaine, et cela conformément aux dispositions de l'article 72.

19. La couverture des risques maladie et accidents, soit par le maintien du bénéfice de la sécurité sociale, soit à défaut par un régime de remplacement assurant dans la mesure du possible des garanties analogues, l'employeur pouvant assurer directement ces garanties.

20. Le maintien ou la compensation des prestations familiales.

21. Le principe de la réintégration du salarié dans sa société d'origine.

22. La réintégration dans des conditions au moins équivalentes à celles du départ.

Les conditions particulières de transport, de résidence et de couverture des risques maladie et accident nos 11, 12, 16, 17 et 19 des paragraphes a) et b) seront explicitées en fonction des conditions particulières de déplacement.

## c) Enfin, l'ordre de mission devra obligatoirement comprendre les dispositions suivantes en cas de déplacement dans les pays présentant des risques politiques ou climatiques dangereux.

23. La couverture des risques politiques et sociaux ; l'application et le respect des législations et règlements de police locaux ; le règlement des conflits ; la garantie du rapatriement en cas d'expulsion ou de départ forcé d'un territoire où se dérouleront des événements tels qu'un retour immédiat devient nécessaire ; les frais de voyage de retour du salarié et, le cas échéant, de son conjoint et de ses enfants mineurs ne resteraient à sa charge que si l'intéressé ou un membre de sa famille était reconnu notoirement responsable de cette situation.

24. Les conditions particulières de travail.

25. Les précautions à prendre contre les maladies spécifiques du pays et les soins particuliers à exercer.

26. Les conditions particulières d'application de la fiscalité, du contrôle des changes, des transferts de fonds.

L'ordre de mission visé ci-dessus est établi sous la condition suspensive que le salarié aura satisfait à toutes les formalités préalables (telles que visas, autorisations de séjour et de travail, contrôle médical, vaccinations, etc.) prévues par la législation en vigueur, tant dans la métropole que dans le pays lieu du déplacement et dont la responsabilité incombe à l'employeur.

# Conditions suspensives et durée des séjours

# Article 67

_En vigueur étendu_

Au cours de la mission, la durée de chaque séjour ne peut, en principe, excéder 20 mois, non compris les
délais de route.

Toutefois, dans le cas où l'ordre de mission se réfère, pour fixer la durée du séjour du salarié, à la durée du
marché pour lequel le salarié a été engagé ou affecté, la durée de ce séjour pourra être prolongée.

Cependant, s'il apparaissait en cours d'exécution du marché que la durée de ce dernier devait atteindre ou
dépasser 24 mois, le salarié pourrait :

- soit bénéficier au cours des 24 mois d'un congé supplémentaire à prendre sur place et terminer ensuite le

  programme pour lequel il a été engagé avant d'être rapatrié pour bénéficier de son congé contractuel ;

- soit demander à être rapatrié pour bénéficier à son lieu de résidence habituel de son congé contractuel,

  auquel cas son ordre de mission pourra être soit renouvelé, soit modifié, soit même annulé.

# Période d'essai

# Article 68

_En vigueur étendu_

Lorsqu'un salarié aura été engagé spécialement pour accomplir des missions hors de France métropolitaine, il
pourra être soumis à la période d'essai prévue par la convention collective ;

- soit en France métropolitaine avant le départ en mission ;

- soit hors de France métropolitaine au lieu de la mission ou au siège de la succursale dont dépend la mission,

  auquel cas si l'essai n'est pas jugé concluant par l'une ou l'autre des parties, le salarié sera rapatrié aux frais de

  l'employeur.

De plus, dans le cas où cette période d'essai s'accomplit hors de France métropolitaine, le salarié ne pourra,
sauf autorisation spéciale de l'employeur, faire venir sa famille pour le rejoindre tant que la période d'essai ne
sera pas achevée de façon concluante.

# Rupture du contrat de travail pendant la mission

# Article 69

_En vigueur étendu_

Dans bien des cas, la procédure de notification de la rupture du contrat de travail par pli recommandé avec
accusé de réception ne pourra être valablement utilisée à l'étranger.

Aussi par dérogation, la notification de la rupture du contrat de travail pendant le cours de la mission à
l'étranger sera reconnue comme valable par le reçu signé par le destinataire de l'écrit l'informant de la rupture
ou, à défaut, par une attestation signée de deux témoins ayant assisté à la remise de l'écrit de résiliation, ou
encore par signification par tout autre moyen authentique.

# Voyages et transports

# Article 70

_En vigueur étendu_

A. - Définition des frais de voyage

Les frais de voyage comprennent dans les limites fixées par l'ordre de mission :

- les frais de transport des personnes et des bagages du lieu de résidence habituelle du salarié au lieu de

  mission, et vice versa ;

- les frais éventuels de subsistance et d'hébergement pendant le voyage.

B. - Déplacement de la famille du salarié

L'ordre de mission devra préciser s'il est possible, et sous quelles conditions, à la famille du salarié, c'est-à-
dire son conjoint et ses enfants mineurs vivant habituellement avec lui, de le suivre dans son déplacement.

Toutefois, l'ordre de mission pourra prévoir une durée minimale en deçà de laquelle le voyage de la famille
ne sera pas à la charge de l'employeur, de même, une durée minimale du séjour effectif de la famille en deçà
de laquelle les frais de voyage ne seront pas payés.

Enfin, l'ordre de mission précisera s'il est possible que certains membres de la famille puissent, pour des
raisons personnelles impératives, bénéficier d'un départ retardé ou d'un retour anticipé.

C. - Conditions d'application des frais de voyage du salarié

Sous les réserves précisées ci-dessus, l'employeur a la charge des frais de voyage et, le cas échéant, de sa
famille :

a) En début et en fin contractuels de la mission ;

b) Lorsque le salarié a réuni le temps de séjour effectif nécessaire pour lui ouvrir le droit à congé en France ;

c) S'il arrive que le salarié soit rappelé pour une période militaire de réserve obligatoire, non provoquée et
sans possibilité de report (sous déduction des remboursements de frais par l'autorité militaire) ;

d) Lorsque des motifs graves de santé, dûment certifiés par prescription médicale avec contre-visite
éventuelle, imposent le retour du salarié ou d'un membre de sa famille (auquel cas seule la famille sera
rapatriée). La contre-visite laissée à l'appréciation de la compagnie d'assurance ou de l'employeur devra avoir
lieu, au plus tard, au lieu d'embarquement ;

e) En cas de décès du salarié au lieu de déplacement, sous la condition que le rapatriement du corps et,
éventuellement, de la famille du salarié intervienne dans les trois mois du décès, sauf cas de force majeure
imposant un délai plus long ;

f) En toute circonstance dont le salarié ou un membre de sa famille ne serait pas reconnu responsable à
l'origine et rendant impossible la prolongation du séjour du salarié au lieu du déplacement. Cette disposition
s'appliquerait en cas d'expulsion ou du départ forcé d'un pays où se déroulent des événements tels qu'un
retour immédiat devient nécessaire ;

g) En cas de licenciement du salarié pour un motif autre que celui de faute grave.

D. - Démission. - Licenciement pour faute grave

En cas de rupture du contrat au lieu de déplacement due soit à la démission du salarié, soit à une faute grave
de sa part, soit à toute circonstance dont il est reconnu à l'origine responsable, l'employeur a la charge des
frais de voyage au prorata du temps de séjour effectué.

Cette clause ne fait pas obstacle à ce que, à la demande de l'intéressé, l'employeur couvre la totalité des frais
effectifs de voyage et exige, après le rapatriement, le remboursement par le salarié de la part de ces frais lui
incombant.

Par ailleurs, dans les cas visés au présent paragraphe, le salarié qui ne sollicite pas son rapatriement en fin de
préavis peut faire valoir auprès de son ancien employeur ses droits en matière de voyage et de transport dans
un délai maximal de 3 mois à compter du jour de la cessation du travail.

E. - Frais de transport des personnels

Sauf stipulation contraire, les conditions de voyages et transports sont déterminées suivant les moyens du
présent article.

Le salarié qui use d'une voie ou de moyens de tranport plus coûteux que ceux agréés par l'employeur n'est
défrayé par celui-ci qu'à concurrence des frais occasionnés par la voie et les moyens régulièrement choisis ou
agréés par l'employeur.

Si le salarié use d'une voie ou de transports plus économiques, il ne peut prétendre qu'au remboursement des
frais engagés, sauf accord entre les parties.

Les classes de passage du salarié et de sa famille seront fixées comme suit, sauf stipulation contraire :

a) En avion (classe touriste) ;

b) En bateau et train 2e classe ou confort équivalent pour les ETAM ; en bateau et train 1re classe ou confort
équivalent pour les I.C.

F. - Bagages

En ce qui concerne le transport des bagages du salarié et de sa famille, il n'est prévu que la franchise
accordée par la compagnie de transport à chaque titre de passage, sauf en ce qui concerne le voyage de
début et le voyage de fin de mission. Dans ces cas, la prise en charge par l'employeur des bagages en sus des
franchises attachées aux billets sera fixée par l'ordre de mission ou règlement spécifique.

Le vol et la perte des bagages pendant le voyage aller et le voyage retour, tant du salarié que de sa famille,
sont également couverts par une assurance dans la limite où la garantie des transporteurs ne s'appliquerait
pas.

G. - Délais de route

Les délais de route sont les délais nécessaires pour se rendre du lieu de résidence habituelle au lieu de
mission, et vice versa par les moyens de transport choisis et agréés par l'employeur.

Le salarié qui use d'une voie ou de moyens de transport moins rapides que ceux agréés par l'employeur ne
peut prétendre de ce fait à des délais de route plus longs.

Si le salarié use d'une voie ou de moyens plus rapides, il continue à bénéficier, en plus de la durée du congé
proprement dit, des délais qui auraient été nécessaires avec l'usage de la voie ou des moyens choisis par
l'employeur.

Les délais de route ne pourront venir en déduction des congés. Ils seront rémunérés comme temps de travail,
suivant des modalités à préciser dans l'ordre de mission.

# Congés

# Article 71

_En vigueur étendu_

Le droit de jouissance au congé est acquis après une durée de séjour effectif, variable suivant les territoires.

Lorsque le salarié prend l'initiative d'un retour anticipé à son travail avant l'expiration normale de son temps
de congé, le paiement des jours de congé non effectivement utilisés n'est pas dû par l'employeur.

Le salarié est libre de prendre son congé dans le pays de son choix, sous réserve, notamment, que
l'employeur ne soit tenu de payer les frais de voyage que jusqu'à concurrence de ce qu'aurait coûté le voyage
du lieu de mission au lieu de sa résidence habituelle et que les délais de route s'ajoutant à la durée du congé
ne puissent être supérieurs au temps nécessaire au salarié pour se rendre en congé au lieu de sa résidence
habituelle, et, éventuellement, pour en revenir.

Le salarié licencié ou démissionnaire au cours de son congé ne peut exiger d'effectuer son préavis outre-mer
ou à l'étranger.

# Prévoyance - Retraites - Chômage

# Article 72

_En vigueur étendu_

Si le régime général de la sécurité sociale n'est pas maintenu, le salarié et sa famille devront être couverts
avec des garanties analogues à celles du régime général de la sécurité sociale conformément aux dispositions
du point 19 de l'article 66, les taux de cotisations incombant au salarié ne pouvant être augmentés de ce
fait. Le cas échéant, ces dispositions devront couvrir les risques de maladies tropicales pour le salarié et sa
famille.

Le régime volontaire risque vieillesse de la sécurité sociale et le régime des retraites complémentaires seront
maintenus et la charge en sera supportée par le salarié et l'employeur dans les proportions habituelles et les
conditions prévues par la loi.

Quant aux allocations familiales, le salarié aura droit à une indemnité compensatrice à partir du moment où
les allocations familiales auxquelles il aurait droit cesseraient de lui être servies.

En cours de déplacement, dans le cas de maladie ou d'accident graves ou de décès du salarié, l'employeur
donne toutes les facilités, notamment pour le remboursement des frais de transport, à un membre de la
famille de l'intéressé ou toute autre personne désignée par lui pour se rendre auprès de ce dernier.

Les salariés envoyés hors de France métropolitaine seront, sur leur demande, couverts par une assurance,
souscrite par l'employeur, contre les risques d'accidents (décès, incapacité temporaire, invalidité totale ou
partielle), suivant des modalités fixées par le règlement spécifique ou l'ordre de mission, et ceci pendant
toute la durée de la mission, voyages compris, et quels que soient les moyens de transport utilisés.

# Contrôle médical

# Article 73

_En vigueur étendu_

En cas de séjour prolongé à l'étranger, le salarié est tenu, à la demande de l'employeur avant son départ et
dans le mois qui suit son retour à son domicile, de subir, lui et éventuellement les membres de sa famille, un
examen médical auprès d'un praticien dûment spécialisé désigné par l'employeur.

Le salarié devra en outre satisfaire obligatoirement, pour lui-même et sa famille, à la réglementation
française et à celle du pays dans lequel il se rend en matière de vaccinations.

# Titre X : Obligations militaires

# Périodes militaires

# Article 74

_En vigueur étendu_

Les salariés qui ont quitté leur emploi pour effectuer leur service militaire obligatoire (normalement ou par
devancement d'appel) pourront être réembauchés dans les conditions prévues par la loi.

Lorsque l'intéressé aura été réintégré dans son emploi à l'issue de son service militaire obligatoire, le temps
passé dans la société avant son départ pour le service militaire entrera en ligne de compte pour le calcul de
son ancienneté dans la société.

Les périodes militaires de réserve obligatoires ne constituent pas une rupture de contrat de travail et ne
peuvent entraîner une réduction des congés annuels.

Pendant ces périodes, les salariés seront rémunérés sur la base de leur traitement mensuel, déduction faite de
la solde perçue qui devra être déclarée à l'employeur.

# Titre XI : Brevets d'invention et secret professionnel

# Inventions des salariés dans le cadre des activités professionnelles

# Article 75

_En vigueur étendu_

Dispositions générales :

Les règles relatives aux inventions des salariés sont fixées par la loi n° 78-742 du 13 juillet 1978 modifiant et
complétant la loi n° 68-1 du 2 janvier 1968 tendant à valoriser l'activité inventive et à modifier le régime des
brevets d'invention.

Conformément aux dispositions de l'article 1er (alinéa 1) de la loi de 1978, sont réputées appartenir à
l'employeur les inventions faites par le salarié dans l'exécution soit d'un contrat de travail comportant une
mission inventive qui correspond à ses fonctions effectives, soit d'études et de recherches qui lui sont
explicitement confiées.

Les formalités que le salarié et l'employeur doivent effectuer l'un envers l'autre, notamment la déclaration
d'invention du salarié, les communications de l'employeur et l'accord entre le salarié et l'employeur, sont
précisées par le décret n° 79-797 du 4 septembre 1979, modifié par le décret n° 84-684 du 17 juillet 1984.

Le salarié et l'employeur doivent s'abstenir de toute divulgation de nature à compromettre en tout ou en
partie l'exercice des droits conférés par la loi.

Lorsqu'un salarié fait une invention ayant trait aux activités, études ou recherches de l'entreprise, et donnant
lieu à une prise de titre de propriété industrielle par celle-ci, le nom du salarié sera mentionné dans la
demande de brevet ou de certificat d'utilité et reproduit dans l'exemplaire imprimé de la description, sauf s'il
s'y oppose. Cette mention n'entraîne pas, par elle-même, le droit de copropriété.

Rémunération du salarié :

Invention brevetable appartenant à l'employeur :

Si cette invention donne lieu à une prise de brevet par l'entreprise, une prime forfaitaire de dépôt sera
accordée au salarié auteur de l'invention, qu'il ait accepté ou non d'être nommé dans la demande de brevet.

Si, dans un délai de 5 ans, consécutif à la prise du brevet ou du certificat d'utilité, le titre de propriété
industrielle a donné lieu à une exploitation commerciale, le salarié auteur de l'invention a droit à une
rémunération supplémentaire pouvant être versée sous des formes diverses telles que :

- versement forfaitaire effectué en une ou plusieurs fois ;
- pourcentage du salaire ;
- participation aux produits de cession de brevet ou aux produits de licence d'exploitation,

et ceci même dans le cas où le salarié serait en retraite ou aurait quitté la société.

L'importance de cette rémunération sera établie en tenant compte des missions, études et recherches confiées
au salarié, de ses fonctions effectives, de son salaire, des circonstances de l'invention, des difficultés de
la mise au point pratique, de sa contribution personnelle à l'invention, de la cession éventuelle de licence
accordée à des tiers et de l'avantage que l'entreprise pourra retirer de l'invention sur le plan commercial.

Le salarié sera tenu informé par écrit des divers éléments pris en compte pour la détermination de la
rémunération supplémentaire. Le mode de calcul et de verserment de la rémunération ainsi que le début et
la fin de la période de versement feront l'objet d'un accord écrit, sauf dans le cas d'un versement forfaitaire
effectué en une seule fois.

Si l'une des parties le demande, toute contestation portant sur l'article 1er ter de la loi du 13 juillet 1978 sera
soumise à une commission paritaire de conciliation dans les conditions prévues à l'article 68 bis de la même
loi.

Inventions non brevetables :

Ces inventions, ainsi que les innovations émanant des salariés et utilisées par l'entreprise, pourront donner
lieu à l'attribution de primes.

# Création de logiciel

# Article 76

_En vigueur étendu_

Conformément à la législation en vigueur et sauf stipulation contraire, le logiciel créé par un ou plusieurs
employés dans l'exercice de leurs fonctions appartient à l'employeur auquel sont dévolus tous les droits
reconnus aux auteurs.

Toute contestation sur l'application de ces dispositions est soumise au tribunal de grande instance du siège
social de l'employeur.

# Secret professionnel

# Article 77

_En vigueur étendu_

Les salariés s'engagent formellement à ne divulger à qui que ce soit aucun des plans, études, conceptions,
projets, réalisations, logiciels, étudiés dans l'entreprise, soit pour le compte des clients de l'entreprise, soit
pour l'entreprise elle-même, se déclarant liés à cet égard par le secret professionnel le plus absolu. Il en est
de même pour les renseignements, résultats, etc., découlant de travaux réalisés dans l'entreprise, ou constatés
chez les clients.

Une infraction des salariés à cette stricte obligation peut constituer une faute lourde.

Dispositions particulières aux CE :

D'une manière générale les chargés d'enquête sont tenus au secret professionnel et à une obligation de
discrétion à l'égard des tiers tant sur l'organisation de leur travail que sur la nature et les résultats des tâches
qui leur sont confiées, et sur les frais et les informations qu'ils ont eu l'occasion de connaître au cours de
l'accomplissement de leurs travaux.

En particulier, sauf instructions écrites de l'employeur, les chargés d'enquête s'engagent formellement à ne
divulguer à qui que ce soit :

- aucun des documents, questionnaires, tableaux, échantillons, notices, etc., qui leur sont remis par

  l'employeur pour l'exécution des enquêtes ;

- aucun résultat ou donnée d'enquête.

Il s'engage à ne pas révéler :

- l'identité des enquêtés, sauf au personnel qualifié de l'employeur ;

- le nom de la personne physique ou morale pour le compte de qui est faite l'enquête, sauf instructions

  précises de l'employeur.

# Publications

# Article 78

_En vigueur étendu_

Les salariés s'interdisent également de publier, sans l'accord de leur employeur, toute étude basée sur les
travaux réalisés pour l'entreprise ou pour les clients, ni faire état des renseignements, résultats, etc., obtenus
chez les clients.

# Titre XII : Dispositions diverses

# Convention collective et accords d'entreprise antérieurs

# Article 79

_En vigueur étendu_

Les entreprises adaptent les clauses de leurs accords qui s'avéreraient moins favorables aux salariés que
celles de la présente convention.

Les avantages reconnus de la présente convention collective ne peuvent en aucun cas s'interpréter comme
s'ajoutant aux avantages déjà accordés pour le même objet dans certaines entreprises à la suite d'usages ou
d'accords.

Lorsque, à la suite notamment d'une fusion, d'une cession, d'une scission ou d'un changement d'activité, la
présente convention collective est mise en application dans une entreprise dont le personnel était jusqu'alors
régi par une autre convention soit en application d'un accord d'entreprise soit en vertu d'un usage, les
dispositions individuelles, incorporées au contrat de travail, restent applicables. Les clauses collectives
de la convention antérieure font l'objet d'une négociation dans l'entreprise concernée afin de prévoir leur
adaptation aux dispositions conventionnelles nouvellement applicables. La convention antérieure continue de
produire effet jusqu'à l'entrée en vigueur de l'accord qui lui est substitué ou, à défaut, pendant une durée d'un
an à compter de l'expiration du préavis qui précède la dénonciation.

Cette négociation d'adaptation aux nouvelles dispositions a pour objet de mettre en place un statut unique du
personnel et d'éviter ainsi la constitution de deux catégories de personnel, un personnel "ancien" continuant à
bénéficier des clauses antérieures accordées à titre collectif et qui n'évoluent plus et un personnel "nouveau"
auquel s'appliquerait la nouvelle convention.

# Date d'application

# Article 80

_En vigueur étendu_

Les dispositions de la présente convention deviendront applicables à compter du 1er janvier 1988.

# Durée - Dénonciation

# Article 81

_En vigueur étendu_

La présente convention est conclue pour une durée indéterminée. Elle pourra être dénoncée par l'ensemble
des signataires employeurs ou salariés, après un préavis minimal de 6 mois.

Sous peine de nullité, ce préavis devra être donné à toutes les organisations signataires par pli recommandé
avec accusé de réception.

Les signataires qui dénonceront la convention devront soumettre un nouveau texte.

La présente convention restera en vigueur jusqu'à ce qu'un nouveau texte l'ait remplacé. Si dans un délai
maximal de 2 ans, l'accord n'a pu se faire, la convention sera résiliée de plein droit.

Elle pourra également être dénoncée par une des parties signataires après un préavis minimal de 3 mois.
Ce préavis devra être donné dans les mêmes conditions que celles précisées au deuxième alinéa du présent
article.

La présente convention est alors maintenue en vigueur entre les autres parties signataires.

# Révision

# Article 82

_En vigueur étendu_

La présente convention est révisable à tout moment par accord unanime des parties contractantes. Au cas où
l'une des parties présenterait une demande de révision partielle accompagnée d'un projet, les autres parties
seraient tenues de l'examiner et de rendre leur réponse dans un délai maximal de 6 mois.

Au cas où l'accord n'aurait pu se faire dans ce délai, la demande de révision serait réputée caduque. Il
appartiendrait à la partie qui désirerait une révision d'envisager l'application de l'article 81 ci-dessus.

# Adhésion

# Article 83

_En vigueur étendu_

Conformément à l'article L. 132-9 du livre Ier du code du travail toute organisation syndicale de salariés ou
d'employeurs ou un ou plusieurs employeurs pris individuellement, non parties à la convention collective,
pourront y adhérer ultérieurement. Cette adhésion sera valable à dater du jour qui suivra celui de son dépôt
au greffe du conseil des prud'hommes de Paris.

L'organisation syndicale de salariés ou d'employeurs, ou tout autre groupement d'employeurs ou un ou
plusieurs employeurs pris individuellement qui auront décidé d'adhérer à la présente convention dans les

formes précitées devra également en informer les parties contractantes par lettre recommandée avec accusé
de réception.

# Procédure de conciliation

# Article 84

_En vigueur étendu_

a) En cas de réclamation individuelle ou collective relative à l'application des dispositions prévues par la
présente convention, l'employeur dispose d'un délai de huit jours pour répondre à cette réclamation.

b) Passé ce délai, ou en cas de réponse négative dans ledit délai, une tentative de conciliation du litige sera
effectuée par une commission restreinte composée du chef d'entreprise et du ou des intéressés assistés de part
et d'autre d'un représentant des organisations syndicales contractantes de la présente convention.

En cas d'échec de cette tentative de conciliation préliminaire, le dossier sera transmis à la commission
paritaire professionnelle de conciliation définie ci-dessous.

c) La commission paritaire professionnelle de conciliation est composée :

- d'une part, d'un représentant de chacune des organisations syndicales contractantes ;
- d'autre part, d'un nombre égal d'employeurs désignés par SYNTEC et la CICF.

Elle se réunit sur convocation d'une des chambres patronales contractantes.

d) Chacune des commissions précitées devra se réunir dans le délai de quinze jours à compter de celui où elle
aura été saisie par la partie la plus diligente.

# Interprétation de la convention

# Article 85

_En vigueur étendu_

Les divergences qui pourraient se manifester dans un bureau d'études sur l'interprétation d'une clause de
la présente convention seront portées devant une commission paritaire d'interprétation qui se réunira sur
convocation de son président, à la demande d'une des organisations contractantes, dans un délai maximal de
15 jours francs après la réception de cette demande.

Cette commission d'interprétation sera composée de deux représentants de chacune des organisations de
salariés contractantes et d'un nombre égal d'employeurs désignés par SYNTEC et la CICF ; ces représentants
seront désignés par leur organisation pour 1 an.

Chaque organisation contractante de salariés peut s'adjoindre un assistant avec voix consultative.

Les organisations contractantes d'employeurs disposeront d'un nombre d'assistants égal au nombre
d'organisations contractantes de salariés et se les répartiront d'un commun accord.

Chaque organisation contractante de salariés dispose, en cas de vote, de deux mandats.

Les organisations contractantes d'employeurs disposent d'un nombre de mandats égal au total de ceux des
salariés et se les répartissent entre elles d'un commun accord.

La commission pourra :

- soit émettre un avis sur l'interprétation à donner à la clause sur laquelle porte le différend. Si cet avis

  est adopté à la majorité des 3/4 des voix, il fera jurisprudence et sera diffusé sous forme de circulaire

  d'interprétation ;

- soit constater que la rédaction de la clause incriminée est défectueuse et qu'il faut soit la modifier, soit en

  expliquer les modalités d'application par une note paritaire annexée à la convention collective. Dans ce cas,

  la commission d'interprétation, statuant à la majorité simple, rédige un projet de nouvelle rédaction ou note

  sur les modalités d'application et le transmet à la commission paritaire de la convention collective en lui

  demandant la révision, conformément à l'article 82, de la convention collective.

Si dans un premier cas, la majorité prévue (3/4 des voix) n'a pas été atteinte, un procès-verbal signé des
membres de la commission exposera les différents points de vue et sera envoyé aux parties qui ont soulevé le
problème.

Un règlement intérieur déterminera les règles de fonctionnement de cette commission.
